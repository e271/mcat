#!/bin/sh


# Run an executable when a DHCP lease is created or destroyed.
# The arguments sent to the script are "add" or "del", 
# then the MAC address, the IP address and finally the hostname
# if there is one.

HOSTFILE='/etc/dnsmasq.d/hosts.dynamic'
HOSTFILECONF='/etc/dnsmasq.d/hosts.conf'
pidfile='/var/run/dnsmasq/dnsmasq.pid'
domain='cluster.cat'

# enable-dbus
# http://www.thekelleys.org.uk/dnsmasq/docs/DBus-interface

# in order to have static dns entries don't forget
# grep dhcp-host /etc/dnsmasq.d/hosts.conf | while read line; do ip="$(echo $line | cut -d, -f 2)"; name="$(echo $line | cut -d, -f3)"; echo "$name : $ip"; echo "${ip}    ${name}.cluster.cat ${name}" >> /etc/dnsmasq.d/hosts.dynamic; done


logger -p local0.notice -t DNSMASQ "$0: $@"
# DNSMASQ: /var/netboot/config/dnsmasq.sh: del 70:71:bc:2d:a9:8f 10.0.0.13 virt03

SERVER='localhost'
APPPATH='mcat'

msg="DNSMASQ tells: $@"

enc="$(echo ${msg} | tr -d '\n' | xxd -plain | tr -d '\n' | sed 's/\(..\)/%\1/g')"

wget --quiet -O - "http://${SERVER}/${APPPATH}/logme?message=${enc}" 2>&1 > /dev/null

# test -f $HOSTFILE || exit 0
# 
# # escaped ip
# eip="$(echo ${3} | sed 's/\./\\\./g')"
# 
# if [ -z "${4}" ]
# then
# 	hostname="mac$(echo ${2} | sed 's/..:..:..:\(..\):\(..\):\(..\)/\1\2\3/')"
# else
# 	hostname="${4}"
# fi
# # fqdn
# fqdn="${hostname}.${domain}"
# 
# case "${1}" in
# 	(del)
# 		# make dns entry static
# 		if grep -v '^#' ${HOSTFILE} | grep -q -E "( ${fqdn} |^${eip} )"
# 		then
# 			logger -p local0.notice -t DNSMASQ "$0: $@ : found in ${HOSTFILE}"
# 		elif grep -q "^address=/${hostname}/${3}" ${HOSTFILECONF}
# 		then
# 			logger -p local0.notice -t DNSMASQ "$0: $@ : found in ${HOSTFILECONF}"
# 		else
# 			echo "${3} ${fqdn} ${hostname}" >> ${HOSTFILE}
# 			kill -HUP $(cat ${pidfile})
# 		fi
# 		;;
# 	(add)
# 		# remove static dns entry
# 		sed -i "s/^${eip}.*$//" ${HOSTFILE}
# 		kill -HUP $(cat ${pidfile})
# 		;;
# esac
