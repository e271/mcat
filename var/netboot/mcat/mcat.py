#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
#    mcat - modest cluster administration toolkit
#    Copyright (C) 2012 Gabriel Klawitter
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from flask import (Flask, render_template, g, flash, request, abort, redirect, 
        session, url_for)
app = Flask(__name__)
app.config.from_pyfile('/etc/mcatconfig.py')

from time import strftime, localtime, strptime
from sys import argv, stdout


import lib.ldapp as ldapp
# import lib.form as mform
import lib.netboot as netboot
import lib.misc as misc
import lib.api as mcatapi
from lib.logme import LogMe



if 'XCAT_PSQL_DB' in app.config:
    import lib.pgsql as pgsql


log = LogMe(app.logger, debug = app.debug,
        email = app.config['ADMIN'],
        logfile = app.config['LOGFILE'],
        devel = True if __name__ == '__main__' else False)




def logrequest():
    app.logger.info('host %s requested %s' % \
            (request.remote_addr, request.path))



@app.before_request
def before_request():
    g.ldap = ldapp.ldapp( app.config )
    if 'ldap-password' in session and session['ldap-password']:
        g.ldap.bind(session['ldap-password'])
    if not g.ldap():
        flash(g.ldap.error, 'error')
        abort(503)

    g.config = app.config
    # das mit der form ist seit POST scheinbar notwendig
    g.form = request.form

    # logging from everywhere
    g.logger = app.logger

    if app.config['DEBUG']:
        g.hostname = app.config['LDAP_URI']
    else:
        g.hostname = misc.gethostname()

    g.footer = app.config['FOOTER']


@app.after_request
def after_request(response):
    g.ldap.term()
    return response




# @app.route("/", methods=['GET', 'POST', 'DELETE'])
@app.route('/', methods=['GET', ])
def mcat():
    '''
    show the landing page
    '''
    logrequest()

    m = g.ldap.getmachines(extattr = True)
    n = g.ldap.getnetgroups()

    return render_template('overview.html', machines = m, netgroups = n)



@app.template_filter('ldate')
def format_ldap_date(s):
    try:
        ctime = localtime()
        t = strptime(s, "%Y%m%d%H%M%SZ")

        if ctime.tm_yday == t.tm_yday and ctime.tm_year == t.tm_year:
            return strftime('%H:%M', t)
        elif ctime.tm_year == t.tm_year:
            return strftime('%d%b', t)
        else:
            return strftime('%d%b%y', t)
    except:
        return s



machine_view = mcatapi.MachineAPI.as_view('machine_api')
# as long as PUT and DELETE aren't natively supported by modern browsers ...
# app.add_url_rule('/machine/', defaults={'machine': None},
#                          view_func=machine_view, methods=['GET',])
# app.add_url_rule('/machine/', view_func=machine_view, methods=['POST',])
# app.add_url_rule('/machine/<string:machine>', view_func=machine_view,
#                          methods=['GET', 'PUT', 'DELETE'])
app.add_url_rule('/machine/', view_func=machine_view, methods=['POST', 'GET'])
app.add_url_rule('/machine/<string:machine>', view_func=machine_view,
                         methods=['GET', 'POST'])
    

nodegroup_view = mcatapi.NodegroupAPI.as_view('nodegroup_api')
# same as above for machine_api
# app.add_url_rule('/nodegroup/', defaults={'nodegroup': None},
#                          view_func=nodegroup_view, methods=['GET',])
# app.add_url_rule('/nodegroup/', view_func=nodegroup_view, methods=['POST',])
# app.add_url_rule('/nodegroup/<string:nodegroup>', view_func=nodegroup_view,
#                          methods=['GET', 'PUT', 'DELETE'])
app.add_url_rule('/nodegroup/', view_func=nodegroup_view, methods=['POST', 'GET'])
app.add_url_rule('/nodegroup/<string:nodegroup>', view_func=nodegroup_view,
                         methods=['GET', 'POST'])


login_view = mcatapi.LoginAPI.as_view('login_api')
app.add_url_rule('/login', view_func=login_view, methods=['POST', 'GET'])

@app.route("/logout", methods=['GET', ])
def logout():
    session.pop('ldap-password', None)
    app.logger.info('LDAP admin logged out.')
    return redirect(url_for('mcat'))


# path means also accept slashes ...
@app.route("/logme/<path:what>", methods=['GET', ])
def logit(what):
    '''
    log everything i get
    '''

    app.logger.info('host %s sent message: %s' % \
            (request.remote_addr, what))
    
    return ('ACCEPTED\n', 202)

@app.route("/logme", methods=['GET', 'POST'])
def logdata():
    '''
    log post or get data
    '''
    if request.method == 'GET':
        message = request.args.get('message', '')
    else:
        message = request.form.get('message', '')

    return logit(message)




@app.errorhandler(404)
def page_not_found(e):
    app.logger.warning('404: %s' % request.url)
    return render_template('error404.html'), 404

@app.errorhandler(503)
def page_not_found(e):
    app.logger.error('503: %s' % request.url)
    return render_template('error503.html', server = app.config['LDAP_URI']), 503

@app.route("/environment")
def environment():
    '''
    print out server environment
    '''

    logrequest()

    response = '<html>'
    response += '<h3>Environment Variables</h3>'

    response += '<table>'
    for key, val in request.environ.items():
        response += '<tr><td>%s</td><td>%s</td></tr>' % (key, val)
    response += '</table>'

    # see: Incoming Request Data
    # at : http://flask.pocoo.org/docs/api/
    # and : http://werkzeug.pocoo.org/docs/wrappers/

    response += '<h3>Flask Request Data</h3>'
    response += '<table>'
    for what in ('path', 'script_root', 'url', 'base_url', 'url_root'):
        response += '<tr><td>%s</td><td>%s</td></tr>' % (what, 
                eval('request.%s' % what))
    response += '</table>'
    
    response += '<h3>Toolkit Request Data</h3>'
    response += '<table>'
    for what in ('host', 'host_url', 'url', 'base_url', 'url_root', 
        'path', 'query_string', 'remote_addr', 'remote_user', 'script_root', 
        'charset', 'url_charset', 'access_route'):
        response += '<tr><td>%s</td><td>%s</td></tr>' % (what, 
                eval('request.%s' % what))
    response += '</table>'

    return '%s</html>' % response


@app.route("/about")
def about():
    logrequest()
    return render_template("gpl-3.0-standalone.html")


@app.route("/netboot/gpxe.cfg")
def gpxecfg():
    '''
    gpxe configuration file
    '''
    logrequest()
    gpxe = netboot.gpxe( host_url = request.host_url,
                         remote = request.remote_addr )

    return gpxe()
        


@app.route("/netboot/grub.cfg")
def grubcfg():
    logrequest()
    cfg = netboot.grub(request.script_root, server = request.host, node = request.remote_addr)
    if g.ldap():
        profile = g.ldap.getprofile(request.remote_addr)
        return cfg.grubcfg(profile)
    else:
        return cfg.grubcfg()


@app.route("/netboot/default.cfg")
def grubcfg():
    logrequest()
    cfg = netboot.grub(request.script_root, server = request.host, node = request.remote_addr)
    if g.ldap():
        profile = g.ldap.getprofile(request.remote_addr)
        return cfg.grubcfg(profile, fullmenu = False)
    else:
        return '# no profile configured for host %s' % request.remote_addr




@app.route("/netboot/profiles.cfg")
def debiangrubmenu():
    '''
    provide a boot menu for unregistered machines
    '''
    logrequest()
    cfg = netboot.grub(request.script_root, server = request.host, node = request.remote_addr )
    return cfg.profilescfg()




@app.route("/netboot/xcat.cfg")
def xcatprofiles():
    '''
    provide a selection menu for netboot boot profiles of xCAT
    '''
    logrequest()
    cfg = netboot.grub(request.script_root, server = request.host, node = request.remote_addr )

    return cfg.xcatcfg()




@app.route("/dnsmasq/hosts.conf")
def gethosts():
    '''
    provide host declarations for dnsmasq:
    from ldap and pgsql

    dhcp-host=00:1a:92:43:d5:3f,10.0.0.1,node01,set:wheezy
    10.0.0.1 node01.cluster.cat node01
    or
    address=
    -> sudo kill -HUP dnsmasq.pid
    '''

    logrequest()
    dnsmasq = netboot.dnsmasq()

    return dnsmasq.conf()



@app.route("/dnsmasq/refresh")
def dnsmasqrefresh():
    '''
    refresh dnsmasq's hostlist
    '''
    logrequest()
    dnsmasq = netboot.dnsmasq()

    if dnsmasq.refresh():
        # Reset Content
        return ('DONE\n', 205)
    else:
        # raise
        # Service Unavailable
        return ('FAILED\n', 503)



def printb(what):
    print '\033[1m%s\033[0m' % what


if __name__ == "__main__":
    if len(argv) <= 1 and stdout.isatty():
        app.debug = True

        app.logger.info("mcat development started")

        app.run(host = '0.0.0.0', port = 8080)
    else:
        if len(argv) == 1 or argv[1] == 'list':
            l = ldapp.ldapp( app.config )
            m = l.getmachines()
            printb('\n%-20s %-20s %-20s' \
                    % ('nodename', 'ip address', 'mac address'))
            print '-' * 59
            for node in m:
                dn = node[0]
                d = node[1]
                for col in ('cn', 'ipHostNumber', 'macAddress'):
                    try:
                        p = d[col][0]
                        for i in d[col]:
                            if len(i) < len(p):
                                p = i
                        print('''%-20s''' % p),
                    except:
                        print(' ' * 20),
                print
            # DNSMASQ: del 70:71:bc:2d:a9:8f 10.0.0.13 virt03
        elif argv[1] in ('add', 'old', 'del', 'tftp', 'init'):
            app.logger.info('mcat cli [%s]: DNSMASQ: %s' \
                    % ( argv.pop(0), ' '.join(argv)))
        else:
            if stdout.isatty():
                print '''mcat cli [%s]: '%s' unknown command (options '%s')''' \
                        % ( argv.pop(0), argv.pop(0), ', '.join(argv) )
            else:
                app.logger.info('''mcat cli [%s]: '%s' unknown command (options '%s')''' \
                        % ( argv.pop(0), argv.pop(0), ', '.join(argv) ) )


else:
    app.debug = False






# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
