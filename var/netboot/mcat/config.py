#!/usr/bin/python 
#
#
# this is a basic configuration template
# it'll be installed at /etc/mcatconfig.py
# where it may configure the mcat app.


# basic application setup
DEBUG = True
SECRET_KEY = 'Cq\xcaU\xa5\x1f\xaf\x16\xf65A\x87\xb6\xbf\x0c\xfe\xd9\x92"8E\x18vn'

ADMIN = 'webmaster@localhost'

# ldap should take precedence over xcats pgsql
# LDAP_PRIO = True
LDAP_PRIO = False


# cluster facing domain for dns zone of the nodes
DOMAIN='debian.cluster'


# ldap coordinates
LDAP_URI = 'ldap://localhost:389'
LDAP_ADMIN = 'cn=admin,dc=debian,dc=cluster'
# leave out to enable login
# LDAP_ADMIN_PASS = '***********'
LDAP_PW_METHOD = 'ssha'
LDAP_MACHINE_DN = 'ou=machines,dc=debian,dc=cluster'
LDAP_NETGROUP_DN = 'ou=netgroups,dc=debian,dc=cluster'
# delegate machines to human or groups of them (very optional)
LDAP_PEOPLE_DN = 'ou=people,dc=debian,dc=cluster'
LDAP_GROUP_DN = 'ou=groups,dc=debian,dc=cluster'


# debian cluster boot profile location
PROFILE_DIR = '/var/netboot/images'
PROFILE_SUFFIX = 'xz'
PROFILE_WEB = '/profiles'
# kernel parameters might once be overwritten by ldap
DEFAULT_BOOTPARAM = 'rw rootfstype=tmpfs root="%(profile)s" imgurl="%(imgurl)s" ip=eth0 ipv6.disable=1'

# always load grub, even for known machines with asserted profiles
GPXEGRUB = False

# footer of the web application if not in debug mode
FOOTER = 'mail bugs to ' + ADMIN


DNSMASQ_DHCP = '/etc/dnsmasq.d/mcat.conf'
DNSMASQ_HOSTS = '/etc/dnsmasq.d/mcat.hosts'
DNSMASQ_PID = '/var/run/dnsmasq/dnsmasq.pid'

LOGFILE='/var/log/mcat.log'


#
# === optional xcat support ===
#
# # xcat coordinates
# XCAT_NETBOOT = '/opt/centocat/install/netboot'
# XCAT_IP = '10.0.0.254'
# # which is exported by this debian host
# XCAT_WEBROOT = '/xcat/netboot'
# 
# # xcats pgsql server configuration
# XCAT_PSQL_SRV = 'localhost'
# XCAT_PSQL_USR = 'xcatadm'
# XCAT_PSQL_DB = 'xcatdb'
# XCAT_PSQL_PASS = '**********'

# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
