#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
#

import wtforms
from wtforms import validators, fields

# need this for config and ldap
from flask import g, flash

from os.path import isdir

import misc




class Submit(object):
    def __init__(self, message):
        self.message = message

    def __call__(self, form, field):
        '''
        field.data is boolean for whether button was clicked
        '''

class IPAddress(validators.IPAddress):
    '''
    override this odd validator to protect against addresses like 
    999.999.999.999
    - could also check for our subnet.
    '''
    def __call__(self, form, field):
        super(IPAddress, self).__call__(form, field)
        try:
            digits = map(int,field.data.split('.'))
            assert max(digits) <= 255
            assert digits[0] != 0
            assert digits[3] != 0
        except:
            raise validators.ValidationError(self.message)



class Password(object):
    '''
    check the ldap password
    '''

    message = u'LDAP authentication failed.'

    def __call__(self, form, field):
        try:
            g.ldap.bind(field.data)
            assert g.ldap.bound
        except:
            raise validators.ValidationError(self.message)



class PasswordForm(wtforms.Form):
    '''
    log in form
    '''

    passvalidator = [ validators.Required(u'Please supply a password.'),
            validators.Length(max = 1024), Password() ]
    password = fields.PasswordField(u'Password', passvalidator)



class MachineForm(wtforms.Form):
    '''
    validate form data for machine entry manipulation
    '''


    cnvalidator = [ validators.Required(u'machine name required'),
            validators.Regexp(regex = '^[a-z]+[a-z0-9\-\._]+[a-z0-9]+$',
            message = u'invalid characters in machine name') ]
    ipvalidator = [ validators.Optional(), IPAddress( 
                message = u'ip address validation error') ]
            # ipv6 = False
    macvalidator = [ validators.Optional(),
            validators.MacAddress(u'mac address validation error') ]

    descvalidator = [ validators.Optional(),
            validators.Length(max = 1024) ]
    serialvalidator = [ validators.Optional(),
            validators.Length(max = 1024) ]


    # actionvalidator = [Submit(), ]
    # actionbtn = fields.SubmitField( label = u'kinda button',
    #         description = u'what to do',
    #         validators = actionvalidator )


    cn = fields.TextField(u'Common Name', cnvalidator)
    ipHostNumber = fields.TextField(u'IP Address', ipvalidator)
    macAddress = fields.TextField(u'MAC Address', macvalidator)
    


    # kernel parameter
    bootParameter = fields.TextField(u'Kernel Boot Parameter')

    # description - descriptive information, max 1,024 characters
    description = fields.TextAreaField(u'Description', descvalidator)

    serialNumber = fields.TextField(u'Serial Number', serialvalidator)

    # l - locality which this object resides in
    l = fields.TextField(u'Location')
    
    # manager - DN of manager, choices ...
    manager = fields.SelectField(u'Manager')

    # o - organization this object belongs to
    # ou - organizational unit this object belongs to
    # owner - owner (of the object)
    # seeAlso - common supertype of DN attributes
    
    # Note that the choices keyword is only evaluated once, so if you want to 
    # make a dynamic drop-down list, you'll want to assign the choices 
    # list to the field after instantiation. Any inputted choices which are 
    # not in the given choices list will cause validation on the field to 
    # fail. -> see __init__()
    # profiles = [ ('', 'not assigned'), ]
    # profiles += [ (p, p) for p in getprofiles() ]
    # profile = fields.SelectField(u'Boot Profile', choices = profiles)
    profile = fields.SelectField(u'Boot Profile')


    def __init__(self, formdata = None, obj = None, prefix = '', **kwargs):
        # wtforms.Form.__init__(self, formdata, obj, prefix, **kwargs)
        # or equivalently
        super(MachineForm, self).__init__(formdata, obj, prefix, **kwargs)

        self.profile.choices = [('', 'not assigned')] + \
                [ (p, p) for p in self.getprofiles() ]
        self.manager.choices = [ ('', 'not assigned'), ]



    def getprofiles(self):
        '''
        find out which profiles are available
        '''
        profiles = []
        if isdir(g.config['PROFILE_DIR']):
            profiles = misc.getprofiles(g.config['PROFILE_DIR'],
                    g.config['PROFILE_SUFFIX'])
        else:
           flash('check configuration value PROFILE_DIR', 'error')

        # flash('available profiles: ' + ','.join(profiles), 'info')
    
        try:
            xcatprofile = misc.xcatprofiles(g.config['XCAT_NETBOOT'])
            if xcatprofile:
                profiles += xcatprofile
        except:
            pass
        return sorted(profiles)


    def setmanagers(self):
        '''
        find out which people are in management group
        '''

        self.manager.choices += [ (m, m.split(',')[0].split('=')[1]) \
                for m in g.ldap.grouptodn('manager') ]

        if len(self.manager.choices) == 1:
            self.manager.choices = [ ('', 'noone available'), ]


class UniqNetGroup(object):
    '''
    check whether the netgroup already exists
    '''
    def __call__(self, form, field):
        if g.ldap.getnetgroups(field.data):
            raise validators.ValidationError(u'Nodegroup %s already exists' % field.data)



class NetGroupForm(wtforms.Form):
    '''
    validate form data for netgroup entry manipulation
    '''

    ngval = [ validators.Required(u'netgroup name required'),
            validators.Regexp(regex = '^[a-z]+[a-z0-9\-\._]+[a-z0-9]+$',
            message = u'invalid characters in nodegroup name'),
            UniqNetGroup() ]

    netgroup = fields.TextField(u'Common Name', ngval)

    # nodes = fields.SelectMultipleField(u'Nodes')
    # nodes = []

    def __init__(self, formdata = None, obj = None, prefix = '', **kwargs):
        super(NetGroupForm, self).__init__(formdata, obj, prefix, **kwargs)

        def fqdn(dn):
            # try with fqdn
            return dn.split(',')[0].split('=')[1]
        def sn(dn):
            return fqdn(dn).replace('.' + g.config['DOMAIN'], '')

        self.nodes = [ fields.BooleanField(label = sn(m[0]),
            default = '', _form = self, _name = sn(m[0])) \
                    for m in g.ldap.getmachines() ]

        for field in self.nodes:
            # not necessary any more giving _form and _name directly
            # field.bind(_form = self, _name = 'name')
            # try:
            #     field.data
            #     # will become True of False
            #     # form data contains 'y' or ''
            # except AttributeError:
            #     field.process(formdata)
            field.process(formdata)

    def __iter__(self):
        for n in self.nodes:
            yield n
        yield self.netgroup

    def selectedNodes(self):
        ret = []
        for n in self.nodes:
            if n.data:
                ret.append( n.name )
        return ret





# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
