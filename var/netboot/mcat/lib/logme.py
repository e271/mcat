#!/usr/bin/python 
# -*- coding: utf-8 -*-
#

# see:
# http://docs.python.org/dev/library/logging.html

import logging
from logging.handlers import RotatingFileHandler
from logging.handlers import SMTPHandler, SysLogHandler

from time import ctime

from misc import gethostname



class FileFormatter(logging.Formatter):
    '''
    format my logfile

    either give a format string in the constructor or format the output 
    ourselves. record is a dictionary with the following keys:
    filename, funcName, levelname, module, message, name, pathname, process, 
    processName, ...
    '''

    hostname = gethostname()

    def __init__(self, devel):
        super(FileFormatter, self).__init__()
        # ('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        # where message is the compiled msg % args
        self.devel = devel
    def format(self, record):
        return '%s %s %s[%s] %s: %s' % \
                (ctime(), self.hostname if not self.devel else 'DEV',
                        record.module, record.filename,
                        record.levelname, record.msg)



# class LogfileHandler(logging.handlers.RotatingFileHandler):
# class LogfileHandler(logging.handlers.TimedRotatingFileHandler):
class LogfileHandler(logging.handlers.WatchedFileHandler):
    # 1MB
    # maxBytes = 1048576

    # backupCount = 1000
    # when = 'd'
    # interval = 28

    def __init__(self, logfile, level = 'DEBUG', devel = False):
        # TimedRotatingFileHandler
        # super(LogfileHandler, self).__init__(logfile,
        #         LogfileHandler.when,
        #         LogfileHandler.interval,
        #         LogfileHandler.backupCount)
        
        super(LogfileHandler, self).__init__(logfile)

        self.setFormatter(FileFormatter(devel))
        if level == 'INFO':
            self.setLevel(logging.INFO)
        elif level == 'WARNING':
            self.setLevel(logging.WARNING)
        else:
            self.setLevel(logging.DEBUG)



class MailFormatter(logging.Formatter):
    msg = '''
        Message type:       %(levelname)s
        Location:           %(pathname)s:%(lineno)d
        Module:             %(module)s
        Function:           %(funcName)s
        Time:               %(asctime)s

        Message:

        %(message)s
        '''
    def __init__(self):
        super(MailFormatter, self).__init__(self.msg)


class MailHandler(logging.handlers.SMTPHandler):
    smtpServer = '127.0.0.1'
    orgAddr = 'server-error@' + gethostname()
    subject = 'ERROR: mCAT Application Failed'

    def __init__(self, admin):
        super(MailHandler, self).__init__(
                self.smtpServer,
                self.orgAddr,
                admin,
                self.subject)

        self.setLevel(logging.ERROR)
        self.setFormatter(MailFormatter())




class SyslogHandler(logging.handlers.SysLogHandler):
    def __init__(self):
        super(SyslogHandler, self).__init__()
        self.setLevel(logging.WARNING)





class LogMe(object):
    def __init__(self, logger, debug,
            email = None,
            logfile = None,
            devel = False):

        if debug:
            logger.setLevel('DEBUG')
        else:
            logger.setLevel('INFO')


        if logfile:
            lfh = LogfileHandler(logfile, devel = devel)
            logger.addHandler(lfh)

        if not devel:
            if email:
                mh = MailHandler(email)
                logger.addHandler(mh)
        
            try:
                sh = SyslogHandler()
                logger.addHandler(sh)
            except:
                logger.critical('Syslog initialisation failed!')




# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
