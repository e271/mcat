#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
#

import ldap
from ldap import modlist


class ldapp(object):
    '''
    LDAP class for our application
    '''

    auth_required = "AUTHENTICATION REQUIRED"

    def __init__(self, conf):
        self.conf = conf
        self.handle = ldap.initialize(conf['LDAP_URI'])
        self.admin = conf['LDAP_ADMIN']
        self.error = ''
        self.bound = False
        if 'LDAP_ADMIN_PASS' in conf:
            self.passwd = conf['LDAP_ADMIN_PASS']
            self.bind(self.passwd)


    def bind(self, password):
        try:
            self.handle.simple_bind_s(self.admin, password)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
        except:
            self.error = u'Es ist ein Fehler mit LDAP aufgetreten.'
        else:
            self.bound = True
            self.error = ''


    def __call__(self):
        '''
        Check if our connection is successfully established and the last 
        operation succeded, too
        '''
        if self.error:
            return False
        else:
            return True


    def term(self):
        '''
        Terminate the connection to the server
        '''
        try:
            self.handle.unbind()
            self.handle.close()
        except:
            pass


    def _whathappend(self, e):
        '''
        prettify my LDAP errors to messages
        '''
        if isinstance(e, ldap.INVALID_CREDENTIALS):
            return u"Invalid login or password"
        elif isinstance(e, ldap.UNWILLING_TO_PERFORM):
            return u"Problems with the LDAP server."
        elif isinstance(e, ldap.INSUFFICIENT_ACCESS):
            return u"Problems with application to LDAP server configuration."
        else:
            return u"Unexpected problems - try again later."



    def _check_auth(self):
        if not self.bound:
            self.error = self.auth_required
            return False
        else:
            return True




    def getmachines(self, ng = None, extattr = False, dn = ''):
        '''
        retrieve machines from the directory server
        '''

        mdn = self.conf['LDAP_MACHINE_DN']
        if self.conf.has_key('LDAP_NETGROUP_DN'):
            ngdn = self.conf['LDAP_NETGROUP_DN']
        else:
            ngdn = ''

        # on ldap search filters 
        if ngdn and ng:
            search_filter = '''(&(objectClass=device)(| (cn=%s)))''' % ')(cn='.join(self.getnetgroups(ng))
        elif dn and dn.endswith(mdn):
            search_filter = '(&(objectClass=device)(%s))' % dn.split(',')[0]
        else:
            search_filter = '(&(objectClass=device)(cn=*))'


        # retrieveAttributes = None
        # which attributes are needed? just retrieve them.
        retrieveAttributes = ('macAddress', 'ipHostNumber', 'cn', 'bootFile')
        if extattr or dn:
            retrieveAttributes += ('createTimestamp', 'modifyTimestamp')
        if dn:
            retrieveAttributes += ('bootParameter', 'description', 
            'serialNumber', 'l', 'manager')

        try:
            machines = self.handle.search_s(mdn, ldap.SCOPE_SUBTREE, 
                    search_filter, retrieveAttributes)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # raise LookupError
            return {}
        else:
            return machines


    def getpeople(self):
        '''
        get all people of the database
        '''
        if not 'LDAP_PEOPLE_DN' in self.conf or \
                not self.conf['LDAP_PEOPLE_DN']:
            return []
        dn = self.conf['LDAP_PEOPLE_DN']

        search_filter = '(objectClass=account)'
        retrieveAttributes = ('dn',)
        try:
            peeps = self.handle.search_s(dn, ldap.SCOPE_SUBTREE, 
                    search_filter, retrieveAttributes)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # raise LookupError
            return []
        else:
            return [ p[0] for p in peeps ]


    def getgroupmember(self, group):
        '''
        get all members of a group
        '''
        if not 'LDAP_GROUP_DN' in self.conf or \
                not self.conf['LDAP_GROUP_DN']:
            return []

        dn = self.conf['LDAP_GROUP_DN']
        search_filter = '''(&(objectClass=posixGroup)(cn=%s))''' % group
        retrieveAttributes = ('memberUid',)
        try:
            members = self.handle.search_s(dn, ldap.SCOPE_SUBTREE, 
                    search_filter, retrieveAttributes)
            return members[0][1]['memberUid']
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # raise LookupError
            return []
        except:
            return []


    def usrtodn(self, users):
        '''
        convert a list of users to their dns if they exist
        '''
        if not users:
            return []
        
        dn = self.conf['LDAP_PEOPLE_DN']

        search_filter = '(&(objectClass=posixAccount)(|%s))' % \
                ''.join(['(cn=%s)' % u for u in users])
        retrieveAttributes = ('dn',)

        try:
            peeps = self.handle.search_s(dn, ldap.SCOPE_SUBTREE, 
                    search_filter, retrieveAttributes)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # raise LookupError
            return []
        else:
            return [ p[0] for p in peeps ]


    def grouptodn(self, group):
        '''
        check members of group and return existing dns
        '''

        if not group:
            return []

        member = self.getgroupmember(group)

        if not member:
            return []

        return self.usrtodn(member)



    def getnetgroups(self, netgroup = ''):
        '''
        retrieve a list of netgroups. if netgroup (ng) is given return its
        members.
        '''

        if self.conf.has_key('LDAP_NETGROUP_DN'):
            ngdn = self.conf['LDAP_NETGROUP_DN']
        else:
            return []

        if netgroup:
            search_filter = '(&(objectClass=nisNetgroup)(cn=%s))' % netgroup
        else:
            search_filter = '(&(objectClass=nisNetgroup)(cn=*))'

        retrieveAttributes = ('cn', 'memberNisNetgroup')

        try:
            ngroups = self.handle.search_s(ngdn, ldap.SCOPE_SUBTREE, 
                    search_filter, retrieveAttributes)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # raise LookupError
            return []

        try:
            if netgroup:
                return ngroups[0][1]['memberNisNetgroup']
        except:
            # might happen if netgroup does not exist
            return []


        nglist = []
        try:
            for ng in ngroups:
                nglist.append((ng[1]['cn'][0], ng[1]['memberNisNetgroup']))
        except:
            raise LookupError
        
        return nglist



    def getprofile(self, ip):
        '''
        get the profile assigned to that ip
        '''

        mdn = self.conf['LDAP_MACHINE_DN']

        search_filter = '(&(objectClass=device)(ipHostNumber=%s)(bootFile=*))' % ip
        retrieveAttributes = ('bootFile',)

        try:
             profile = self.handle.search_s(mdn, ldap.SCOPE_SUBTREE, 
                                             search_filter, retrieveAttributes)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # raise LookupError
            return ''

        try:
            # looks like
            # [('cn=localhost.cluster.cat,ou=machines,dc=cluster,dc=cat', {'bootFile': ['wheezy']})]
            return profile[0][1]['bootFile'][0]
        except:
            return ''

    def addmachine(self, data):
        '''
        add a machine to the directory

        data['cn']
        data['macAddress']
        data['ipHostNumber']
        data['profile']

        original ldif layout:

        dn: cn=gpu02.cluster.cat,ou=machines,dc=cluster,dc=cat
        objectClass: top
        objectClass: device
        objectClass: bootableDevice
        objectClass: ipHost
        objectClass: ieee802Device
        bootFile: wheezy.cuda
        ipHostNumber: 10.0.0.22
        macAddress: 00:25:90:03:95:06
        cn: gpu02.cluster.cat
        cn: gpu02
        '''

        if not self._check_auth():
            return False

        mdn = self.conf['LDAP_MACHINE_DN']

        shortname = data['cn'].split(' ')[0].split('.')[0]
        domain = '.'.join([mdn.split(',')[1].split('=')[1], 
                mdn.split(',')[2].split('=')[1]])
        fqdn = '.'.join([shortname, domain])
        dn = 'cn=%s,%s' % (fqdn, mdn)

        modlist = [ ( 'objectClass', ['top', 'device', 'bootableDevice', 
        'ipHost', 'ieee802Device' ] ),
                    ( 'bootFile', [str(data['profile'])]),
                    ( 'ipHostNumber', [str(data['ipHostNumber'])]),
                    ( 'macAddress', [str(data['macAddress'])]),
                    ( 'cn', [ str(fqdn), str(shortname) ]) ]

        for attr in ('bootParameter', 'serialNumber', 'l', 'manager', 
                'description'):
            if attr in data and data[attr]:
                if attr == 'manager':
                    modlist.append( ( 'manager', map(str, data[attr].split(', '))) )
                else:
                    modlist.append( ( attr, str(data[attr])) )


        try:
             self.handle.add_s(dn, modlist)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            self.error = dn
            # raise e
            # raise LookupError
            return False

        return True


    def delmachine(self, dn):
        '''
        remove a machine from ldap directory
        - check at least that its a machine ...
        '''

        if not self._check_auth():
            return False

        if not dn.endswith(self.conf['LDAP_MACHINE_DN']):
            self.error = dn
            return False


        try:
            self.handle.delete_s(dn)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            self.error = dn
            # raise e
            # raise LookupError
            return False
        return True


    def modmachine(self, dn, data):
        '''
        change a machine's record
        '''
              
        if not self._check_auth():
            return False

        mdn = self.conf['LDAP_MACHINE_DN']
        
       
        if not dn.endswith(mdn):
            self.error = dn
            return False

        oldmachine = self.getmachines(dn = dn)
        if not oldmachine or not oldmachine[0][1]:
            return False

        shortname = data['cn'].split(' ')[0].split('.')[0]
        domain = '.'.join([mdn.split(',')[1].split('=')[1], 
                mdn.split(',')[2].split('=')[1]])
        fqdn = '.'.join([shortname, domain])
        newdn = 'cn=%s,%s' % (fqdn, mdn)



        if dn != newdn:
            # ok machines name should also be changed - delete'n'add
            # doesn't add the extended attributes right now. TODO
            if self.addmachine(data):
                return self.delmachine(dn)
            else:
                return False


        # don't know whether i need them
        # modlist = [ ( 'objectClass', ['top', 'device', 'bootableDevice', 
        #                                 'ipHost', 'ieee802Device' ] ) ]

        # damn profile instead of bootFile, argh!
        attrset = ('ipHostNumber', 'macAddress', 'bootParameter', 'profile', 
                'serialNumber', 'l', 'manager', 'description')
 
        attr = {}
        for d in data:
            if d in attrset:
                if d in ( 'description', 'serialNumber', 'l' ):
                    attr[d] = str(data[d])
                elif d == 'bootParameter':
                    # must be: key=server:path -> path wird genommen
                    attr[d] = str(data[d])
                elif d == 'profile':
                    attr['bootFile'] = map(str, data[d].split(', '))
                else:
                    attr[d] = map(str, data[d].split(', '))
        

        ignore_attr = [ 'createTimestamp', 'modifyTimestamp', 'cn' ]


        ml = ldap.modlist.modifyModlist(oldmachine[0][1], attr, 
                ignore_attr_types = ignore_attr )

        # modify attributes
        try:
             self.handle.modify_s(dn, ml)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            self.error = dn
            # raise e
            # raise LookupError
            return False

        return True
       



    def addnetgroup(self, ng, nodes):
        '''
        original ldif layout:
        
        dn: cn=all,ou=netgroups,dc=debian,dc=cluster
        objectClass: top
        objectClass: nisNetgroup
        cn: all
        memberNisNetgroup: node01
        memberNisNetgroup: node02
        memberNisNetgroup: node03
        '''

        if not self._check_auth():
            return False

        if self.conf.has_key('LDAP_NETGROUP_DN'):
            ngdn = self.conf['LDAP_NETGROUP_DN']
        else:
            self.error = "LDAP_NETGROUP_DN not configured"
            return False
        mdn = self.conf['LDAP_MACHINE_DN']

        cn = str(ng)
        dn = 'cn=%s,%s' % ( ng, ngdn )


        if not dn or not cn:
            self.error = 'data not recognized (%s, [%s])' \
                    % ( ng, str(nodes))
            return False

        if not nodes:
            self.error = 'no nodes selected (%s)' % ng
            return False

        modlist = [ ( 'objectClass', ['top', 'nisNetgroup' ] ),
                    ( 'cn', [ cn ] ),
                    ( 'memberNisNetgroup' , nodes ) ]

        try:
             self.handle.add_s(dn, modlist)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # self.error = dn
            # raise e
            # raise LookupError
            return False

        return True



    def delnetgroup(self, dn):
        '''
        remove a netgroup from ldap directory
        - check at least that its a machine ...
        '''
        
        if not self._check_auth():
            return False

        if self.conf.has_key('LDAP_NETGROUP_DN'):
            ngdn = self.conf['LDAP_NETGROUP_DN']
        else:
            self.error = "LDAP_NETGROUP_DN not configured"
            return False

        if dn and not dn.endswith(ngdn):
            dn = 'cn=%s,%s' % (dn, ngdn)

        try:
            self.handle.delete_s(dn)
        except ldap.LDAPError,e:
            self.error = self._whathappend(e)
            # self.error = dn
            # raise e
            # raise LookupError
            return False
        return True







# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
