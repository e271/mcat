#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
#

from os import walk
from os.path import isdir
import glob

def human_readable(n):
    '''
    return a human readable string for a number of bytes
    '''

    try:
        n = int(n)
    except:
        return 'nan'

    if n >= 1073741824:
        return '%sG' % (int(n / 1073741824))
    elif n >= 1048576:
        return '%sM' % (int(n / 1048576))
    elif n >= 1024:
        return '%sK' % (int(n / 1024))
    else:
        return n
        


def getprofiles(folder, suffix):
    '''
    find out which profiles are available
    '''
    profiles = []
    if isdir(folder):
        for archive in glob.glob( '%s/*.%s' % (folder, suffix)):
            profiles.append('.'.join(archive.split('/')[-1].split('.')[0:-1]))

    return sorted(profiles)
    



def xcatprofiles(pdir):
    '''
    return a list of netboot boot profiles of xCAT
    '''

    
    if isdir(pdir):
        r = []
    else:
        return []
    
    for root, dirs, files in walk(pdir):
        if 'rootimg' in dirs:
            # don't descent in image trees resp. stop here we've found one
            dirs.remove('rootimg')
            if set(['kernel', 'initrd-stateless.gz', 'rootimg.gz']) <= set(files):
                # issubset()
                try:
                    nil, osys, arch, profile = root.split(pdir)[1].split('/')
                    if arch != 'x86_64':
                        r.append('xcat-%s-%s-%s' % (osys, arch, profile))
                    else:
                        r.append('xcat-%s-%s' % (osys, profile))
                except:
                    pass

    return r




def getmacfromarp(ip):
    arp_cache = '/proc/net/arp'
    mac = ''

    with open(arp_cache) as arp:
        for line in arp:
            if line.count(ip):
                # here we go
                v = line.split()
                mac = v[3]
                break
    return mac
         


def gethostname():
    from socket import getfqdn
    return getfqdn()



# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
