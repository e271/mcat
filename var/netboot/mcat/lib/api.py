#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
#

from flask.views import MethodView
from flask import render_template, abort, g, flash, redirect, url_for, session

# http://werkzeug.pocoo.org/docs/datastructures/
from werkzeug.datastructures import MultiDict

import misc
import form as mform
from netboot import dnsmasq



class NodegroupAPI(MethodView):

    def get(self, nodegroup = None):
        if nodegroup is None:
            # return a list of nodegroups
            
            # this is for the menue here
            ng = g.ldap.getnetgroups()

            # mdict = MultiDict()
            # mdict.add('netgroup', 'whoisit')
            # f = mform.NetGroupForm(mdict)
            try:
                f = mform.NetGroupForm(session['nodegroup_data'])
                session.pop('nodegroup_data', None)
                session.pop('nodegroup_name', None)
            except KeyError:
                f = mform.NetGroupForm()

            if g.config['DEBUG']:
                g.footer = '### ' + str(f.data) + ' ###'

            return render_template('nodegroups.html',
                    netgroups = ng, form = f)
        else:
            # expose a single nodegroup
            m = g.ldap.getmachines(nodegroup, extattr = True)
            n = g.ldap.getnetgroups()

            return render_template('overview.html',
                    machines = m,
                    netgroups = n,
                    headline = 'Nodegroup ' + nodegroup,
                    nodegroup = nodegroup )

    def post(self, nodegroup = None):
        # create a new nodegroup
        # since PUT and DELETE are not supported by modern standards they are 
        # separated here one by one and will call the proper method manually.
        if nodegroup:
            # this must be an edit request - or delete ...
            if 'DELETE' in g.form:
                return self.delete(nodegroup)
            else:
                return self.put(nodegroup)


        if not g.ldap.bound:
            session['nodegroup_data'] = g.form
            return redirect(url_for('login_api'))
        

        # actually unnecessary since my validators will work it out
        # if not 'POST' in g.form:
        #     # a true POST
        #     abort(404)


        f = mform.NetGroupForm(g.form)
        if g.config['DEBUG']:
            flash('Nodegroup POST received', 'info')
            g.footer = '### '
            # g.footer += str(g.form)
            g.footer += '%s = %s : ' % (f.netgroup.name, f.netgroup.data)
            for n in f.selectedNodes():
                g.footer += repr(n) + ', '
            g.footer += ' ###'


        if not f.validate() \
                or not g.ldap.addnetgroup( f.data['netgroup'], f.selectedNodes() ):
            if g.ldap.error:
                flash('Nodegroup POST  of %s failed (%s).' \
                        % (f.data['netgroup'], g.ldap.error), 'error')

            # this is for the menue here
            ng = g.ldap.getnetgroups()
            return render_template('nodegroups.html',
                    netgroups = ng, form = f)

        flash('Nodegroup POST  of %s succeed' % f.data['netgroup'], 'info')


        return self.get(f.data['netgroup'])




    def delete(self, nodegroup):
        # delete a single nodegroup
        if not g.ldap.bound:
            session['nodegroup_name'] = nodegroup
            return redirect(url_for('login_api'))

        if g.ldap.delnetgroup(nodegroup):
            flash('Netgroup %s DELETEed' % nodegroup, 'info')
        else:
            flash('DELETEtion of netgroup %s failed' % nodegroup, 'error')

        return self.get()


    def put(self, nodegroup):
        # update a single nodegroup
        abort(404)






class MachineAPI(MethodView):


    def __init__(self):
        # so find my machines and give them to the template
        # netgroup = form['netgroup'] if 'netgroup' in request.form else None
        # TODO filtering doesn't work since we're not handling this via 
        # POST anymore
        # self.machines = g.ldap.getmachines(g.form.get('netgroup'))
        # self.machines = g.ldap.getmachines()
        
        # collect netgroups for the menubar
        self.netgroups = g.ldap.getnetgroups()






    # this overrides method based dispatching ...
    # def dispatch_request(self, machine = None):

    def dispatch_list(self, form = {}):
        self.machines = g.ldap.getmachines()
        if g.config['DEBUG']:
            try:
                debugstring = "form.data: %s" % str(form.data)
            except:
                debugstring  = '<b>DEBUG</b><br />'
                debugstring += '<p><b>ldap machines:</b>'
                for m in self.machines:
                    debugstring += '<br /><i>%s</i><br /> %s' % (m[0], str(m[1]))
                # debugstring += '</p><p><b>ldap people:</b><br />'
                # for p in g.ldap.getpeople():
                #     debugstring += '<i>%s</i>, ' % p
                debugstring += '</p><p><b>manager group members :</b> '
                for m in g.ldap.grouptodn('manager'):
                     debugstring += '<i>%s</i>, ' % m
                if g.ldap.error:
                    debugstring += '</p><p><strong>%s</strong>' % g.ldap.error
                debugstring += '</p>'

            g.footer = debugstring

        # strip out fqdns if both short and long name are available
        for m in self.machines:
            for n in m[1]['cn']:
                if n.endswith(g.config['DOMAIN']) and n.split('.')[0] in m[1]['cn']:
                    m[1]['cn'].remove(n)
                    break

        return render_template('machines.html', machines = self.machines,
                form = form, netgroups = self.netgroups)
 


    def dispatch_machine(self, machine, form):
        # expose a single machine
        return render_template('machine_detail.html',
                form = form, dn = machine)
 


    def get(self, machine = None):
        '''
        HTTP GET
        '''

        if machine is None:
            # return a list of machines
            try:
                session.pop('machine_name', None)
                formdata = session.pop('machine_data')
                form = mform.MachineForm(formdata, manager = '')
                return self.dispatch_list(form)
            except KeyError:
                return self.dispatch_list()
        else:
            # expose a single machine
            m = g.ldap.getmachines(dn = machine)
            if not m:
                # we don't know that machine
                abort(404)
                # ('cn', 'ipHostNumber', 'macAddress', 'bootFile', 
                # 'bootParameter', 'serialNumber', 'l', 'manager', 
                # 'description')
            try:
                session.pop('machine_name', None)
                formdata = session.pop('machine_data')
                form = mform.MachineForm(formdata)
            except KeyError:

                dndict = m[0][1]

                # strip out fqdns if both short and long name are available
                for n in dndict['cn']:
                    if n.endswith(g.config['DOMAIN']) and \
                            n.split('.')[0] in dndict['cn']:
                        dndict['cn'].remove(n)
                        break

                # gather data to feed the form
                mdict = MultiDict()
                for key, val in dndict.items():
                    if isinstance(val, list):
                        for v in val:
                            if key == 'bootFile':
                                mdict.add('profile', v)
                            else:
                                mdict.add(key, v)
                    else:
                        if key == 'bootFile':
                            mdict.add('profile', val)
                        else:
                            mdict.add(key, val)


                form = mform.MachineForm(mdict)

            form.setmanagers()
            return self.dispatch_machine(machine, form)
            
        abort(404)



    def post(self, machine = None):
        # create a new machine

        # since PUT and DELETE are not supported by modern standards they are 
        # separated here one by one and will call the proper method manually.
        if machine:
            # this must be an edit request - or delete ...
            if 'DELETE' in g.form:
                return self.delete(machine)
            else:
                return self.put(machine)


        if not g.ldap.bound:
            session['machine_data'] = g.form
            return redirect(url_for('login_api'))


        # so no machine is given - we'll take this as an add request

        if 'AddMachine' in g.form:
            if g.config['DEBUG']:
                flash('AddMachine request received', 'info')
            g.AddMachine = True
            # establish the form, so that the template can use it
            form = mform.MachineForm(g.form)
            return self.dispatch_list(form)

        if 'POST' in g.form:
            # there's no option to give a manager here
            form = mform.MachineForm(g.form, manager = '')

            if g.config['DEBUG']:
                flash('AddMachine POST received', 'info')

            if not form.validate() or not g.ldap.addmachine(form.data):
                flash('AddMachine POST  of %s failed.' % form.data['cn'], 'error')
                # gebe doch form object mit, weil fehler drin seien koennen
                g.AddMachine = True
                return self.dispatch_list(form)

            flash('AddMachine POST  of %s succeed' % form.data['cn'], 'info')

            if dnsmasq().refresh():
                flash('AddMachine succeed - dnsmasq declarations refreshed', 'info')
            else:
                flash('Dnsmasq declaration refreshment failed', 'error')
            return self.dispatch_list()

        # don't accept other data
        abort(404)


    
    
    

    def delete(self, machine):
        # delete a single machine

        if not g.ldap.bound:
            session['machine_name'] = machine
            return redirect(url_for('login_api'))


        if g.config['DEBUG']:
            flash('DELETE request received: %s' % machine, 'info')

        if not g.ldap.delmachine(machine):
            flash('Machine removal failed: %s' % g.ldap.error, 'error')
        else:
            if dnsmasq().refresh():
                flash('Machine removal succeed - dnsmasq declarations refreshed', 'info')
            else:
                flash('Machine removal succeed - dnsmasq update failed', 'error')
        return self.dispatch_list()


    def put(self, machine):
        # update a single machine
        # http://www.amundsen.com/examples/put-delete-forms/

        if not g.ldap.bound:
            session['machine_name'] = machine
            session['machine_data'] = g.form
            return redirect(url_for('login_api'))


        if g.config['DEBUG']:
            flash('PUT request received: %s' % machine, 'info')

        form = mform.MachineForm(g.form)
        form.setmanagers()
        if not form.validate():
            flash('Machine processing (PUT)  of %s failed.' % form.data['cn'], 'error')
            # gebe doch form object mit, weil fehler drin seien koennen
            return self.dispatch_machine(machine, form)

        # o'key this will handle ldap from now on
        if not g.ldap.modmachine(machine, form.data):
            flash('Machine processing (PUT)  of %s failed because of LDAP, ' \
                    'sorry.' % form.data['cn'], 'error')
        else:
            if dnsmasq().refresh():
                flash('Machine modification succeed - dnsmasq declarations refreshed', 'info')
            else:
                flash('Machine modification succeed - dnsmasq update failed', 'error')

        return self.dispatch_list()



class LoginAPI(MethodView):

    def get(self, login = True):
        form = mform.PasswordForm(g.form)

        return render_template("login.html", form = form, 
                ldapadmin = g.config['LDAP_ADMIN'] )


    def post(self):
        form = mform.PasswordForm(g.form)

        if not form.validate():
            # flash("Authentication failed.", 'error')
            g.logger.info('Authentication to LDAP server %s failed for %s.' \
                    % ( g.config['LDAP_URI'], g.config['LDAP_ADMIN']))
            return render_template("login.html", form = form, 
                    ldapadmin = g.config['LDAP_ADMIN'] )

        g.logger.info('Authentication to LDAP server %s successful (%s).' \
                % ( g.config['LDAP_URI'], g.config['LDAP_ADMIN']))
        
        flash("Authentication successful.", 'info')
        session['ldap-password'] = form.data['password']

        target = url_for('mcat')
        if 'nodegroup_name' in session:
            target = url_for('nodegroup_api') + session['nodegroup_name']
            session.pop('nodegroup_name', None)

        elif 'nodegroup_data' in session:
            target = url_for('nodegroup_api')

        elif 'machine_name' in session:
            target = url_for('machine_api') + session['machine_name']
            session.pop('machine_name', None)

        elif 'machine_data' in session:
            target = url_for('machine_api')
            

        return redirect(target)





# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
