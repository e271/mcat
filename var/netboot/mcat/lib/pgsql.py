#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
#
# http://initd.org/projects/psycopg

import psycopg2

import misc


class pgsql(object):
    '''
    PostgreSQL class for our application
    '''
    def __init__(self, conf):
        self.error = ''
        self.debug = conf['DEBUG']
        self.host = conf['XCAT_PSQL_SRV']
        self.db = conf['XCAT_PSQL_DB']
        self.usr = conf['XCAT_PSQL_USR']
        self.pwd = conf['XCAT_PSQL_PASS']
        self.c = None


    def connect(self):
        try:
            self.conn = psycopg2.connect(host = self.host,
                    database = self.db,
                    user = self.usr,
                    password = self.pwd)
            self.c = self.conn.cursor()
        except Exception, e:
            self.error = errorcodes.lookup(e.pgcode)
        except:
            self.error = u'Es ist ein Fehler mit PostgreSQL aufgetreten.'


    def __call__(self):
        '''
        Check if our connection is successfully established and the last 
        operation succeded, too
        '''
        if self.error:
            return False
        else:
            return True


    def term(self):
        '''
        Terminate the connection to the server
        '''
        try:
            self.c.close()
            self.conn.close()
        except:
            pass




    def findnode(self, mac = None, ip = None, name = None):
        '''
        find node in xCATs database
        - results always in a dict with this tripple
        '''
        
        r = {}

        if ip:
            r['ip'] = ip
            mac = misc.getmacfromarp(ip)
            if not mac:
                r['debug'] = 'IP not in arp cache'
            else:
                r['mac'] = mac

                # q = '''SELECT * FROM mac ''' \
                #         '''WHERE mac = '%s' AND ''' \
                #         '''(disable is NULL OR disable != '1');''' % r['mac']
                q = '''SELECT mac.node, hosts.ip ''' \
                        ''' FROM mac, hosts ''' \
                        ''' WHERE mac.mac = '%s' ''' \
                        ''' AND (mac.disable is NULL OR mac.disable != '1') ''' \
                        ''' AND mac.node = hosts.node;''' % r['mac']
                self.c.execute(q)

                for rec in self.c:
                     r['name'] = rec[0]
                     r['ip'] = rec[1]
                     r['disabled'] = False

                if 'name' not in r:
                    r['debug'] = 'MAC not in xCAT mac table'

        return r


    def identify(self, mac = None, curip = None, name = None):
        '''
        identify a xCAT node
        '''

        r = {}

        if not mac and curip:
            try:
                mac = misc.getmacfromarp(curip)
                assert mac
            except:
                return {}
        elif not mac and name:
            try:
                from DNS import dnslookup
                ip = dnslookup(name=name, qtype='A')[0]
                mac = misc.getmacfromarp(ip)
                assert mac
            except:
                return {}

        # so we have a mac address
        q = '''SELECT mac.node, hosts.ip, nodetype.os, nodetype.arch, nodetype.profile ''' \
                ''' FROM mac, hosts, nodetype, nodelist ''' \
                ''' WHERE mac.mac = '%s' ''' \
                ''' AND (mac.disable is NULL OR mac.disable != '1') ''' \
                ''' AND (hosts.disable is NULL OR hosts.disable != '1') ''' \
                ''' AND (nodetype.disable is NULL OR nodetype.disable != '1') ''' \
                ''' AND (nodelist.disable is NULL OR nodelist.disable != '1') ''' \
                ''' AND mac.node = hosts.node ''' \
                ''' AND mac.node = nodetype.node ''' \
                ''' AND mac.node = nodelist.node; ''' % mac
        self.c.execute(q)

        try:
            col = ['name', 'ip', 'os', 'arch', 'profile']
            for record in self.c:
                for index, item in enumerate(col):
                    r[item] = record[index]
                break
            assert r
            r['disabled'] = False
        except:
            return {}


        return r


    def getnodes(self):
        '''
        return a dict of dicts {name : {ip, mac}}
        '''

        if not self.c:
            raise LookupError

        r = {}

        q = '''SELECT nodelist.node, hosts.ip, mac.mac ''' \
                ''' FROM mac, hosts, nodelist ''' \
                ''' WHERE (mac.disable is NULL OR mac.disable != '1') ''' \
                ''' AND (hosts.disable is NULL OR hosts.disable != '1') ''' \
                ''' AND (nodelist.disable is NULL OR nodelist.disable != '1') ''' \
                ''' AND mac.node = hosts.node ''' \
                ''' AND mac.node = nodelist.node; '''
        self.c.execute(q)

        for record in self.c:
            r[record[0]] = { 'ip' : record[1],
                    'mac' : record[2],
                    'comment' : 'XCAT'}

        return r




# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
