#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
#


from os.path import getsize, realpath, basename, isdir, exists
from os import stat
from time import strftime, localtime
from urllib import quote_plus
import lib.misc as misc

from flask import g



class gpxe(object):
    '''
    initial gpxe config file creation

    - deliver gpxe.cfg with the proper content for the profile assigned 
      through ldap
    - if the node doesn't exist (no ip/mac), provide a memu of profiles and 
      insert that node with ip/mac into the ldap directory.
    '''

    def __init__(self, host_url, remote = ''):
        # where the images are hosted
        self.webpath = g.config['PROFILE_WEB']
        self.url = host_url + self.webpath
        self.host = host_url
        self.remote = remote
        self.suffix = g.config['PROFILE_SUFFIX']
        self.boot_param = g.config['DEFAULT_BOOTPARAM']
        self.grubmenu = g.config['GPXEGRUB']


    def __call__(self, profile = None, bootparam = ''):
        if profile:
            self.profile = profile
        else:
            self.profile = g.ldap.getprofile(self.remote)

        if bootparam:
            self.boot_param = bootparam

        
        if self.grubmenu:
            r = ('#!gpxe',
                 'set 210:string %s' % self.host,
                 'chain ${210:string}grub/grub2pxe.0',
                 '# request sent from %s (%s)' % \
                         (self.remote, repr(self.profile) if self.profile else 'no default profile'),
                 '')
            return '\n'.join(r)

        if self.profile and self.profile.startswith('xcat-'):
            # send xCAT configuration
            try:
                server = 'http://${next-server}'
                xc = xcat()
                # p = xc.getprofile(self.remote)
                c = xc.confprofile(self.profile)
                r = ('#!gpxe', 
                    'kernel %s%s %s' % (server, c['kernel'], c['append']),
                    'initrd %s%s' % (server, c['initrd']), 'boot', '')
                return '\n'.join(r)
            except:
                raise
                return '#!gpxe\nhdboot\n'
        elif self.profile:
            bootparam = self.boot_param % \
                        { 'profile' : self.profile,
                          'imgurl' : '%s/%s.%s' % (self.url, self.profile, self.suffix) }
            gpxecfg = ('#!gpxe',
                          'initrd %s/%s/initrd.img' % (self.url, self.profile),
                          'kernel %s/%s/vmlinuz %s' % (self.url, self.profile, bootparam),
                          'boot',
                          '# profile %s requested from %s \n' % (self.profile, self.remote))
            return '\n'.join(gpxecfg)
        else:
            # exit pxeboot
            return '#!gpxe\nhdboot\n'

        


class grub(object):
    '''
    Grub class for our application
    '''
    

    grubmenu = {}
    
    grubmenu['initial'] = [ '# grub initial configuration\n',
            'set default=0', 'set timeout=10', '\n',
            'terminal_output gfxterm', '\n\n' ]

    grubmenu['defaults'] = [ '\n\n', 'set menu_color_normal=cyan/blue',
                             'set menu_color_highlight=white/blue', '\n\n' ]


    grubmenu['localdisk'] = [ 'menuentry "Boot your local harddrive (hd0)" {',
            '        insmod chain',
            '        insmod relocator',
            '        insmod mmap',
            '        chainloader (hd0)+1',
            '}\n\n\n' ]

    grubmenu['debian'] = [ 'submenu "> Debian cluster profiles" {',
            'configfile ${apppath}/netboot/profiles.cfg',
            '}\n\n\n' ]

    grubmenu['xcat'] = [ 'submenu "> Centocats xCAT profiles" {',
            'configfile ${apppath}/netboot/xcat.cfg',
            '}\n\n\n' ]

    grubmenu['reboot'] = [ 'menuentry "Reboot" {',
            '        echo "Rebooting your machine ..."',
            '        reboot',
            '}\n\n\n' ]
    
    grubmenu['exit'] = [ 'menuentry "Exit grub" {',
            '        echo "booting next device ..."',
            '        exit',
            '}\n\n\n' ]

    grubmenu['specials'] = [ 'submenu "> Specials" {',
            '        configfile      /grub/specials.cfg',
            '}\n\n\n' ]




    def __init__(self, approot, node, server):
        self.debug = g.config['DEBUG']
        self.ldapprio = g.config['LDAP_PRIO']
        self.webpath = g.config['PROFILE_WEB']
        self.suffix = g.config['PROFILE_SUFFIX']
        self.pdir = g.config['PROFILE_DIR']
        self.default_boot_param = g.config['DEFAULT_BOOTPARAM']
        self.server = server
        self.node = node
        self.grubmenu['root'] = [  '\nset root="(http,%s)"\n' % self.server, ]
        self.approot = approot

        try:
            # check out xcat configuration
            self.xcat = xcat(node)
        except:
            self.xcat = None

        apppathcmd = 'set apppath="%s"\n\n' % self.approot
        
        # don't know why i need this
        for cfg in ('debian', 'xcat'):
            if not apppathcmd in self.grubmenu[cfg]:
                self.grubmenu[cfg].insert(1, apppathcmd)
        # but it makes this obsolete
        if not apppathcmd in self.grubmenu['initial']:
            self.grubmenu['initial'].append(apppathcmd)

        
       


    def getbootmenuentry(self, profile):
        '''
        create a grub boot menue entry for that profile
        '''

        if not profile:
            return ''

        e = '\n'

        if profile.startswith('xcat-'):
            # create a xcat entry ex. xcat-centos6-compute
            e += self.xcatcfg(profile)
        else:
            # create a debian entry ex. wheezy
            e += self.profilescfg(profile)

        e += '\n'
        return e



    def __call__(self):
        '''
        get'em all?
        '''
        return self.grubcfg


    def grubcfg(self, profile = '', fullmenu = True):
        '''
        create first grub config
        '''

        r = '# automagic grub menu creation by mCAT\n'

        if fullmenu:
            for pt in ('initial', 'root', 'defaults'):
                r += '\n'.join(self.grubmenu[pt])

        # either no profile or xcats pgsql takes precedence
        if self.xcat and ( not profile or not self.ldapprio):
            try:
                p = self.xcat.getprofile(self.node)

                assert p
                r += '# we have an active xcat node here: %s\n' % self.node
                r += self.getbootmenuentry(p)
            except:
                r+= '''# but it's not properly configured\n'''
            
            if self.debug:
                r+= '# debug\n# %s\n' % repr(self.xcat.node)


        r += self.getbootmenuentry(profile)

        if fullmenu:
            for pt in ('debian', 'xcat', 'specials', 'localdisk', 
                'reboot', 'exit'):
                if pt == 'xcat' and not self.xcat:
                    continue
                r += '\n'.join(self.grubmenu[pt])
        return r



    def profilescfg(self, configured = ''):
        '''
        provide a boot menu for unregistered machines
        '''
        if configured:
            response = '\n'
            profiles = [configured,]
        else:
            response = '\n'.join(self.grubmenu['defaults'])
            profiles = misc.getprofiles(self.pdir, self.suffix)

        for profile in profiles:
            try:
                rpath = realpath('%s/%s/vmlinuz' 
                        % (self.pdir, profile))
                bname = basename(rpath)
                kver = bname.split('vmlinuz-')[1]
                response += "\n# kernel version: %s\n" % kver
                imgpath = realpath('%s/%s.%s' % ( self.pdir, profile, 
                    self.suffix))
                psize = misc.human_readable(getsize(imgpath))
                mtime = localtime(stat(imgpath).st_mtime)
                ctime = localtime()
                if ctime.tm_yday == mtime.tm_yday and ctime.tm_year == mtime.tm_year:
                    mdate = strftime('%H:%M', mtime)
                elif ctime.tm_year == mtime.tm_year:
                    mdate = strftime('%d%b', mtime)
                else:
                    mdate = strftime('%d%b%y', mtime)

                imgurl = 'http://%s%s/%s.%s' % ( self.server, self.webpath, 
                        profile, self.suffix)

                if configured:
                    desc = ('CONFIGURED (%s): Debian %s, with Linux %s (%s, %s)' 
                        % (self.node, profile, kver, psize, mdate))
                else:
                    desc = 'Debian %s, with Linux %s (%s, %s)' % (profile, kver, 
                        psize, mdate)
                bootdir = '%s/%s' % (self.webpath, profile)

                response += self.grubentry(profile, desc, bootdir, imgurl)

            except:
                response += "\n# profile '%s' doesn't seem to exist\n" % profile
                if configured:
                    response += '\nmenuentry "Missing default profile %s'\
                            ' for %s" {\n' \
                            '    echo "Check LDAP entry for host %s and '\
                            'missing profile %s."\n'\
                            '    sleep 3\n}\n' % (configured, self.node, 
                                    self.node, configured)
                else:
                    continue
        response += '\n\n\n# request from %s \n' % self.node
        return response


    
    def grubentry(self, profile, desc, bootdir, imgurl = '', bootparam = None):
        '''
        create grub boot menu entry
        '''

        bootparamdict = { 'profile' : profile,
                          'imgurl' : imgurl }
        if not bootparam: 
            # bootparam = 'rw rootfstype=tmpfs root=myrootfs ip=eth0'
            bootparam = self.default_boot_param % bootparamdict
        else:
            bootparam = bootparam % bootparamdict

        if not imgurl:
            bootparam = bootparam.replace('imgurl=','')


        r  = '\n\nmenuentry "%s"' % desc
        r += ' --class debian --class gnu-linux --class gnu --class os {\n'

        # log this menuentry is selected
        msg = quote_plus('''seleceted menuentry '%s' ''' % desc, safe = '')
        r += '    test -s %s/logme?message=%s\n' % ( self.approot, msg)

        r += '    echo    "Loading %s ..."\n' % desc
        r += '    linux   %s/vmlinuz %s\n' % (bootdir, bootparam)
        r += '    echo    "Loading initial ramdisk ..."\n'
        r += '    initrd  %s/initrd.img\n' % bootdir

        # log this menuentry is now being booted
        msg = quote_plus('''booting menuentry '%s' ''' % desc, safe = '')
        r += '    test -s %s/logme?message=%s\n' % ( self.approot, msg)
        r += '}\n'

        return r




    def xcatcfg(self, configured = ''):
        '''
        provide a selection menu for netboot boot profiles of xCAT
        '''


        if not self.xcat:
            r = '\n'.join(self.grubmenu['defaults'])
            return r + 'menuentry "xCAT configuration error" {\n' \
                   '    echo "Check your xCAT configuration."\n'\
                   '    sleep 3\n}\n'
   

        if not isdir(self.xcat.netboot):
            r = '\n'.join(self.grubmenu['defaults'])
            return r + 'menuentry "xCAT configuration error" {\n' \
                   '    echo "xCAT profile directory \'%s\' not found'\
                   ' (host: %s)."\n}\n' % (self.xcat.netboot, self.node)
       
        if configured:
            # we're creating a single default boot menue entry
            r = '\n'
            profiles = [configured,]
        else:
            # we're creating a grub submenu
            r = '\n'.join(self.grubmenu['defaults'])
            profiles = misc.xcatprofiles(self.xcat.netboot)

 
        if configured:
            r += '# centocat boot profile %s at %s:\n' % (configured, 
                    self.xcat.netboot)
        else:
            r += '# centocat boot profiles at %s:\n\n\n' % self.xcat.netboot
   

        for profile in profiles:
            c = self.xcat.confprofile(profile)
            if c:
                r += '# '.join(c['comment'])
                r += '\n'
                classes = '--class centos --class  gnu-linux --class gnu --class os'

                if configured:
                    r += 'menuentry "CONF(%s): %s" %s {\n' % (self.node, c['title'], classes)
                else:
                    r += 'menuentry "%s" %s {\n' % (c['title'], classes)

                # log this menuentry is selected
                msg = quote_plus('''seleceted menuentry '%s' ''' % c['title'], safe = '')
                r += '    test -s %s/logme?message=%s\n' % ( self.approot, msg)

                r += '    echo    "Loading Linux (%s) ..."\n' % c['title']
                r += '    linux   %s %s\n' % (c['kernel'], c['append'])


                r += '    echo    "Loading initial ramdisk ..."\n'
                r += '    initrd  %s\n' % c['initrd']

                # log this menuentry is now being booted
                msg = quote_plus('''booting menuentry '%s' ''' % c['title'], safe = '')
                r += '    test -s %s/logme?message=%s\n' % ( self.approot, msg)
                r += '\n}\n\n\n'

            else:
                if not configured:
                    r += "\n# profile '%s' doesn't seem to exist\n" % profile
                    continue
                else:
                    r += '\nmenuentry "Missing default profile %s'\
                         ' for %s" {\n' \
                         '    echo "Check LDAP entry for host %s and '\
                         'missing profile %s."\n'\
                         '    sleep 3\n}\n' % (configured, self.node, 
                                                self.node, configured)
             
        r += '\n\n\n# request from %s \n' % self.node
        return r



class dnsmasq(object):
    '''
    Dnsmasq class for hosts definitions
    '''
    
    def __init__(self):
        self.debug = g.config['DEBUG']
        self.ldapprio = g.config['LDAP_PRIO']
        self.domain = g.config['DOMAIN']
        try:
            self.dhcpleases =  g.config['DNSMASQ_DHCP']
            self.hostsfile = g.config['DNSMASQ_HOSTS']
            self.pid =  g.config['DNSMASQ_PID']
        except:
            pass

        
        if set(['XCAT_PSQL_USR', 'XCAT_PSQL_SRV', 'XCAT_PSQL_DB', 
            'XCAT_PSQL_PASS']) <= set(g.config.keys()):
            import pgsql
            self.xcatdb = pgsql.pgsql( g.config )
            self.xcatdb.connect()
            xcatnodes = self.xcatdb.getnodes()
            self.xcatdb.term()
        else:
            xcatnodes = {}


        ldapnodes = {}
        for n in g.ldap.getmachines():
            name = ''
            for nn in n[1]['cn']:
                if nn.count('.') == 0:
                    name = nn
                    break
            if not name:
                name = n[1]['cn'][0].split('.')[0]

            ldapnodes[name] = { 'ip' : n[1]['ipHostNumber'][0],
                    'mac' : n[1]['macAddress'][0] \
                            if 'macAddress' in n[1] else '',
                    'comment' : "LDAP"}


        # so we have two dicts of nodes and we have to merge them

        if self.ldapprio:
            nodes = ldapnodes
            mergein = xcatnodes
        else:
            nodes = xcatnodes
            mergein = ldapnodes

        for name,data in mergein.items():
            if not data['ip']:
                # omit incomplete host definitions
                continue
            ips = [ h['ip'] for h in nodes.values()]
            macs = [ h['mac'] for h in nodes.values()]

            if data['ip'] in ips or ( data['mac'] and data['mac'] in macs):
                # some coordinates already exist
                continue

            if not name in nodes.keys():
                nodes[name] = { 'ip' : data['ip'],
                        'mac' : data['mac'],
                        'comment' : data['comment']}
            elif name in nodes.keys() \
                    and not ( nodes[name]['ip'] and nodes[name]['mac'] ):
                nodes[name]['ip'] = data['ip']
                nodes[name]['mac'] = data['mac']
                nodes[name]['comment'] += ' merged with %s' % data['comment']

        self.nodes = nodes


    def conf(self, host_record = True):
        conf = ''
        for name,data in self.nodes.items():
            if not data['ip']:
                conf += '# omitting incomplete host definition for %s (%s)\n' \
                        % (name, data['comment'])
                continue

            conf += '\n# host defined by %s\n' % data['comment']
            if host_record:
                conf += 'host-record=%s,%s.%s,%s\n' \
                        % (name, name, self.domain, data['ip'])
            conf += 'txt-record=%s.%s,"%s"\n' % (name, self.domain, data['comment'])
            if data['mac']:
                conf += 'dhcp-host=%s,%s,%s\n' % (data['mac'], data['ip'], name)
        

        return conf


    def hosts(self):
        conf = ''
        for name,data in self.nodes.items():
            if not data['ip']:
                conf += '# omitting incomplete host definition for %s (%s)\n' \
                        % (name, data['comment'])
                continue

            conf += '\n# host defined by %s\n' % data['comment']
            conf += '%s    %s %s.%s\n' % (data['ip'], name, name, self.domain)
        

        return conf



    def refresh(self):
        '''
        refresh dnsmasq's hostlist
        '''
        try:
            assert self.dhcpleases
            assert self.hostsfile
            assert self.pid
    
            with open(self.dhcpleases, 'w') as out:
                out.write(self.conf())
            with open(self.hostsfile, 'w') as out:
                out.write(self.hosts())
    
            with open(self.pid, 'r') as pidfile:
                pid = int(pidfile.readline())
    
            try:
                from os import kill
                # send SIGHUP to dnsmasq
                kill(pid, 1)
            except:
                from os import system
                system("sudo -u dnsmasq /bin/kill -SIGHUP %s" % pid)
    
            return True
        except:
            # raise
            return False



class xcat(object):
    '''
    create a dict with kernel, initrd, append for profile
    '''

    kfile = 'kernel'
    ifile = 'initrd-stateless.gz'
    rfile = 'rootimg.gz'

    def __init__(self, node = ''):

        # check out xcat configuration
        try:
            self.ip = g.config['XCAT_IP']
            self.bootparam = ('XCAT=%s:3001 netdev=eth0 ipv6.disable=1' % 
                    g.config['XCAT_IP'])
            self.web = g.config['XCAT_WEBROOT']
            self.netboot = g.config['XCAT_NETBOOT']

            assert set(['XCAT_PSQL_USR', 'XCAT_PSQL_SRV', 'XCAT_PSQL_DB', 
                'XCAT_PSQL_PASS']) <= set(g.config.keys())
            import pgsql
            self.db = pgsql.pgsql( g.config )

        # except KeyError:
        except:
            raise EnvironmentError

        self.node = node

        
 
    def getprofile(self, node):
        self.db.connect()
        # node = self.db.findnode( ip = self.node)
        # node = self.db.identify( mac = '00:25:90:67:d6:e8')
        # search for hostname but has to have an arp cache entry!
        # node = self.db.identify( name = 'node01')
        ndict = self.db.identify( curip = node)
        # ndict['name'] is never used ...
        self.db.term()
        
        if not 'disabled' in ndict or ndict['disabled'] == False:
            try:
                p = '-'.join(['xcat', ndict['os'],
                    ndict['arch'] if ndict['arch'] else 'x86_64',
                    ndict['profile']])
                return p
            except:
                return ''
        return ''
     

    def confprofile(self, profile):

        conf = {}
        try:
            p = profile.split('-')
            if len(p) == 3:
                osys = p[1]
                arch = 'x86_64'
                xprofile = p[2]
            elif len(p) == 4:
                osys = p[1]
                arch = p[2]
                xprofile = p[3]
            else:
                raise ValueError

            if p[0] != 'xcat':
                raise ValueError

            netbootdir = '%s/%s/%s/%s' % (self.netboot, osys, arch, xprofile)
            kernel = '%s/%s' % (netbootdir, self.kfile)
            initrd = '%s/%s' % (netbootdir, self.ifile)
            rootimg = '%s/%s' % (netbootdir, self.rfile)

            if not exists(kernel) or not exists(initrd) \
                    or not exists(rootimg):
                raise IOError

            reldir = '/'.join([osys, arch, xprofile])
            webdir = '%s/%s' % (self.web, reldir)
            
            conf['comment'] = [ '', ]
            conf['comment'].append('os: %s, arch: %s, profile: %s\n' % \
                    (osys, arch, xprofile))

            rootimgsize = misc.human_readable(getsize(rootimg))
            conf['comment'].append('(kernel %s, initrd %s, rootimg %s)' % \
                            (misc.human_readable(getsize(kernel)), \
                            misc.human_readable(getsize(initrd)), \
                            rootimgsize))
            

            conf['title'] = '%s : profile %s (%s)' % (osys, xprofile, rootimgsize)



            conf['kernel'] = '%s/%s' % (webdir, self.kfile)

            imgstr = ('imgurl=http://%s/install/netboot/%s/%s' %
                        (self.ip, reldir, self.rfile))
            mac = misc.getmacfromarp(self.node)
            ifname = 'ifname=eth0:%s' % mac if mac else ''

            conf['append'] = '%s %s %s' % (imgstr, ifname, self.bootparam)

            conf['initrd'] = '%s/%s' % (webdir, self.ifile)

            return conf
 
        except:
            raise
            return {}









# vim: tw=78 fo=w2tqc ts=4 ft=python shiftwidth=4 expandtab
