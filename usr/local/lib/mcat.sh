#!/bin/sh
# 
#     mcat - modest cluster administration toolkit
#     Copyright (C) 2012 Gabriel Klawitter
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# 

[ -r /etc/default/mcat ] || exit 1

# source configuration variables
. /etc/default/mcat


DEBIAN_IMG_DIR="${NETBOOT_DIR}/images"
CONFIG_PATH="${NETBOOT_DIR}/config/images"
NETGROUPDN="${OU_NETGROUP},${BASEDN}"
MACHINEDN="${OU_MACHINE},${BASEDN}"
SCHROOT_CONF="/etc/schroot/schroot.conf"


if ! [ -d "$DEBIAN_IMG_DIR" ]
then
	echo "please configure profile directory in /etc/default/mcat"
	exit 1
fi

[ "${MASTER}" ] || MASTER="$(hostname -f)"
[ "${CLUSTERNAME}" ] || CLUSTERNAME="unspecified"
[ "${BASEDN}" ] || BASEDN="$(ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// \
	-b "olcDatabase={1}hdb,cn=config" olcSuffix \
	| grep '^olcSuffix:' | cut -d ' ' -f 2 )"


LC_ALL=C
LANG=C

# test "$SHELL" = "/bin/zsh" && setopt shwordsplit
test "$(ps --no-headers -p $$ | tr -s ' ' | cut -d ' ' -f 4)" = "zsh" && setopt shwordsplit

if [ -t 1 ]
then
        # running in a terminal - argument is the fd
        BOLD='\033[01m'
        DEF='\033[00m'
        EBOLD='\x1b[01m'
        EDEF='\x1b[00m'
fi

_printb () {
        printf "${BOLD}${*}${DEF}"
}

nodelist ()
	# list available nodes
{
	if [ -z "$NETGROUPDN" ] || [ -z "$MACHINEDN" ]
	then
		echo "please configure ldap settings in /etc/default/mcat in order to use this function"
		exit 1
	fi
	if [ "$1" ]
	then
		NODELIST="$(ldapsearch -x -LLL -b $NETGROUPDN "(cn=$1)" 'memberNisNetgroup' | grep '^memberNisNetgroup' | cut -d ' ' -f 2 | tr '\n' ' ')"
		if [ -z "${NODELIST}" ]
		then
			if [ "$(ldapsearch -x -LLL -b ${MACHINEDN} "(cn=$1)" 'cn')" ]
			then
				NODELIST="$1"
			fi
		fi
		echo "NODELIST = ${NODELIST}"

	else
		echo "Available netgroups:"
		ldapsearch -x -LLL -b $NETGROUPDN '(cn=*)' 'cn' | grep '^cn' | cut -d ' ' -f 2
	fi
}


_confipmi ()
	# check ipmi configuration and read account data
{
	if [ "${IPMI_DOMAIN}" -o "${IPMI_SUFFIX}" ] && [ -x "/usr/bin/ipmitool" ] \
		&& [ -r "${IPMI_ACCOUNT}" ]
	then
		IPMI_USER="$(cut -d: -f1 < ${IPMI_ACCOUNT})"
		IPMI_PASS="$(cut -d: -f2 < ${IPMI_ACCOUNT})"

		if [ "${IPMI_DOMAIN}" ]
		then
			IPMI_NAME_EXT="${IPMI_SUFFIX}.${IPMI_DOMAIN}"
		else
			IPMI_NAME_EXT="${IPMI_SUFFIX}"
		fi
	fi

	if [ -z "${IPMI_USER}" -o -z "${IPMI_PASS}" ]
	then
		_printb "> IPMI not properly configured. See /etc/default/mcat\n"
		unset IPMI_DOMAIN
	fi
}




nodestat ()
	# check nodes status
{
	_confipmi
	[ "${DOMAIN}" ] || DOMAIN="$(hostname -d)"


	nodelist $1
	for n in ${NODELIST}
	do
		if [ "${IPMI_NAME_EXT}" ]
		then
			_printb "${n}: "
			/usr/bin/ipmitool -I lan -H "${n}${IPMI_NAME_EXT}" \
				-U ${IPMI_USER} -P ${IPMI_PASS} power status | tr -d '\n'
		fi
		if [ -x '/usr/bin/nmap' ]
		then
			/usr/bin/nmap -sS ${n}.${DOMAIN} -p 22 2>/dev/null \
				| sed -r -n 's:^22/tcp open  ssh:, sshd listening:p' | tr -d '\n'
		fi
		echo
	done
}


nodepower ()
	# set nodes power status via ipmi
{
	_confipmi
	if [ -z "${IPMI_NAME_EXT}" ] 
	then
		_printb "> please configure IPMI first"
		exit 1
	fi

	case $2 in
		(status|on|off|cycle|reset|diag|soft)
			nodelist $1
			for n in ${NODELIST}
			do
				_printb "${n}: "
				/usr/bin/ipmitool -I lan -H "${n}${IPMI_NAME_EXT}" \
					-U ${IPMI_USER} -P ${IPMI_PASS} power ${2}
			done
			;;
		(*)
			_printb "> unknown IPMI power command $2"
			exit 1
			;;
	esac
}



pssh ()
	# parallel ssh
{
	if [ -z "$1" ]
	then
		echo "USAGE: $0 <NODELIST> command"
		echo "       NODELIST may either be a comma separated list of hosts or a nodegroup"
	fi
	nodelist $1
	if [ -z "${NODELIST}" ]
	then
		echo "no hosts for execution"
		exit 1
	fi
	shift
	/usr/bin/parallel-ssh --verbose --timeout 0 --inline -H "${NODELIST}" $@ 
}


_mkhostsfile ()
	# create hostsfile for parallel commands
{
	HOSTFILE="$(mktemp)"
	nodelist $1
	if [ -z "${NODELIST}" ]
	then
		echo "no hosts for execution"
		exit 1
	fi
	echo "${NODELIST}" | tr ' ' '\n' > ${HOSTFILE}
}



pscp ()
	# copy files to nodes
{
	if [ -z "$1" ]
	then
		echo "USAGE: $0 <NODELIST> local-file remote-file"
		echo "       NODELIST may either be a comma separated list of hosts or a nodegroup"
	fi
	_mkhostsfile $1
	shift
	/usr/bin/parallel-scp --verbose --hosts ${HOSTFILE} $@
	rm -f ${HOSTFILE}
}

prsync ()
	# rsync files to nodes
{	
	_mkhostsfile $1
	shift
	/usr/bin/parallel-rsync --verbose --archive --hosts ${HOSTFILE} $@
	rm -f ${HOSTFILE}
}

pnuke ()
	# kill processes matchin' pattern on the nodes
{
	_mkhostsfile $1
	shift
	/usr/bin/parallel-nuke --verbose --hosts ${HOSTFILE} $@
	rm -f ${HOSTFILE}
}

pslurp ()
	# retrieve files from the nodes
{
	_mkhostsfile $1
	shift
	if ! [ "$1" = "-L" ] || [ -z "$2" ] || [ -z "$3" ]
	then
		echo "usage:"
		echo "pslurp -L destdir remote local"
		echo "       copies remote file to local destdir with filename local"
	fi
	/usr/bin/parallel-slurp --hosts ${HOSTFILE} $@
	rm -f ${HOSTFILE}
}

_imgavail ()
	# check the available images give either "all" or imagename
{

	test -d ${DEBIAN_IMG_DIR} || return 1
	
	# images="$(find ${DEBIAN_IMG_DIR} -maxdepth 1 -mindepth 1 -type d -printf "%f ")"
	images="$(find ${DEBIAN_IMG_DIR} -mindepth 1 -maxdepth 1 -type d -printf '%f\n' | sort | tr '\n' ' ')"

	# test -z ${images} || return 1

	if [ "${1}" = "all" ] || [ -z "${1}" ]
	then
		image="${images}"
	elif ! [ -d "${DEBIAN_IMG_DIR}/${1}" ]
	then
		echo "image '$1' not found in image dir ${DEBIAN_IMG_DIR}"
		echo "available images are: ${images}"
		unset image
		return 2
	else
		image="$1"
	fi
}



schrootconf ()
	# find chroots to enable for schroot
{
	set -e
	# CONF="/etc/schroot/schroot.conf"
	# DEBIAN_IMG_DIR="/var/netboot/images"
	# XCAT_IMG_DIR="/opt/centocat/install/netboot"
	# OPT_IMG_DIR="/opt"

	[ "${SCHROOT_USERS}" ] || SCHROOT_USERS="root,auser"
	[ "${SCHROOT_GROUPS}" ] || SCHROOT_GROUPS="root,adm"

	if [ -r "${SCHROOT_CONF}" ]
	then
		backup="${SCHROOT_CONF}.$(date +%s)"
		echo -n "> backing up old configuration file "
		mv -v ${SCHROOT_CONF} ${backup}
		# :> ${SCHROOT_CONF}
	fi
	cat > ${SCHROOT_CONF} <<- 'EOF'
	#
	#        _   _           _   _          _ 
	#   __ _| |_| |_ ___ _ _| |_(_)___ _ _ | |
	#  / _` |  _|  _/ -_) ' \  _| / _ \ ' \|_|
	#  \__,_|\__|\__\___|_||_\__|_\___/_||_(_)
	#                                         
	# - this file will be overwritten by the schrootconf utility!
	#
	EOF

	if [ "${DEBIAN_IMG_DIR}" -a -d "${DEBIAN_IMG_DIR}" ]
	then
		cat > ${SCHROOT_CONF} <<- 'EOF'

		#  ___      _    _             _  _     _   _              _   
		# |   \ ___| |__(_)__ _ _ _   | \| |___| |_| |__  ___  ___| |_ 
		# | |) / -_) '_ \ / _` | ' \  | .` / -_)  _| '_ \/ _ \/ _ \  _|
		# |___/\___|_.__/_\__,_|_||_| |_|\_\___|\__|_.__/\___/\___/\__|
		# 
		EOF

		# find debian images first
		find ${DEBIAN_IMG_DIR} -mindepth 1 -maxdepth 1 -type d -printf '%f\n' | {
		while read image
		do
			echo "> adding debian image ${image}"
			cat >> ${SCHROOT_CONF} <<- EOF

			[${image}]
			description=Debian Netboot Image ${image}
			directory=${DEBIAN_IMG_DIR}/${image}
			users=${SCHROOT_USERS}
			root-users=${SCHROOT_USERS}
			groups=${SCHROOT_GROUPS}
			root-groups=${SCHROOT_GROUPS}
			type=directory
			profile=minimal
			setup.nssdatabases=
			preserve-environment=false

			EOF
		done
		}
	else
		echo "!!! Debian netboot directory ${XCAT_IMG_DIR} not found !!!"
		echo "->  please configure NETBOOT_DIR in /etc/default/mcat properly."
	fi

	if [ "${XCAT_IMG_DIR}" -a -d "${XCAT_IMG_DIR}" ]
	then
		cat >> ${SCHROOT_CONF} <<- 'EOF'

		#       ___   _ _____ 
		# __ __/ __| /_\_   _|
		# \ \ / (__ / _ \| |  
		# /_\_\\___/_/ \_\_|  
		#                          
		EOF

		# oder: for d in /opt/centocat/install/netboot/*/*/*/rootimg
		find ${XCAT_IMG_DIR} -mindepth 3 -maxdepth 3 -type d -printf '%P\n' | {
		while read image
		do
			if [ "$(echo ${image} | cut -d '/' -f 2)" != "x86_64" ]
			then
				echo ">> unimplemented architecture found in ${XCAT_IMG_DIR}/${image}"
				continue
			fi
			name="$(echo ${image} | sed 's:/:-:g; s:-x86_64::')"
			echo "> adding xCAT image ${name}"
			cat >> ${SCHROOT_CONF} <<- EOF
			
			[${name}]
			description=xCAT netboot profile ${name} (64bit)
			directory=${XCAT_IMG_DIR}/${image}/rootimg
			users=${SCHROOT_USERS}
			root-users=${SCHROOT_USERS}
			groups=${SCHROOT_GROUPS}
			root-groups=${SCHROOT_GROUPS}
			type=directory
			profile=minimal
			setup.nssdatabases=
			preserve-environment=false

			EOF
		done
		}
	fi


	if [ "${OPT_IMG_DIR}" -a -d "${OPT_IMG_DIR}" ]
	then
		cat >> ${SCHROOT_CONF} <<- 'EOF'

		#   ___  ___ 
		#  / _ \/ __|
		# | (_) \__ \
		#  \___/|___/
		# 
		EOF

		for i in ${OPT_IMG_DIR}/*
		do
			test -d ${i}/bin -a -d ${i}/sbin -a -d ${i}/etc -a -d ${i}/lib \
				-a -d ${i}/proc -a -d ${i}/sys -a -d ${i}/dev \
				-a -d ${i}/var -a -d ${i}/usr || continue
			name="$(basename ${i})"
			echo "> adding system ${name}"

			cat >> ${SCHROOT_CONF} <<- EOF
			
			[${name}]
			description=Operating System ${name}
			directory=${i}
			users=${SCHROOT_USERS}
			root-users=${SCHROOT_USERS}
			groups=${SCHROOT_GROUPS}
			root-groups=${SCHROOT_GROUPS}
			type=directory
			profile=minimal
			setup.nssdatabases=
			preserve-environment=false

			EOF
		done
	fi
		
	set +e
}


enterimage ()
	# chroot into a imagetree with all necessities
{
	
	test -z "${DEBIAN_IMG_DIR}" && return 1
	
	_imgavail ${1}
	
	if [ -z "${image}" ]
	then
		echo -n "Usage: "
		echo "${0} <imagename>"
		echo "    where image name can be one of ${images}"
		return 1
	fi

	test -d "${DEBIAN_IMG_DIR}/${image}" || return 1

	if [ -x "/usr/bin/schroot" ]
	then
		grep -q "^\[${image}\]" ${SCHROOT_CONF} || schrootconf
	fi

	if [ -x "/usr/bin/schroot" ] && grep -q "^\[${image}\]" ${SCHROOT_CONF}
	then
		/usr/bin/schroot -d / -c ${image}
	else
		echo -n "${image}" > ${DEBIAN_IMG_DIR}/${image}/etc/debian_chroot
		
		mount -t proc procfs ${DEBIAN_IMG_DIR}/${image}/proc
		# mount -o bind /sys ${DEBIAN_IMG_DIR}/${image}/sys
		mount -t sysfs sysfs ${DEBIAN_IMG_DIR}/${image}/sys

		chroot ${DEBIAN_IMG_DIR}/${image}

		umount ${DEBIAN_IMG_DIR}/${image}/proc
		umount ${DEBIAN_IMG_DIR}/${image}/sys

		rm -f ${DEBIAN_IMG_DIR}/${image}/etc/debian_chroot
	fi
}


prepimage ()
	# prepare a netboot image tree for this cluster
{
	
	# set -x 

	if ! [ "$USER" = "root" ]
	then
		echo "!!! you need to be root to use that command !!!"
		return 1
	fi

	if [ -z "$1" ]
	then
		echo "Usage: $0 <profile_name> [bootstrap]"
		echo "       either give multiple profiles to apply customization"
		echo "       or one profile that should be bootstrapped"
		return 1
	fi
	
	if [ "${2}" = "bootstrap" ]
	then	
		if ! [ -x "/usr/sbin/debootstrap" ]
		then
			echo "!!! debootstrap not found !!!"
			return 1
		fi

		image="$1"
		if [ -d "${DEBIAN_IMG_DIR}/${image}" ]
		then
			echo ">> there is already a profile named '${image}'"
			return 1
		fi

		if [ "${image}" = "all" ]
		then
			echo ">> profile name 'all' is not possible, sorry."
			return 1
		fi

		includes="linux-image-amd64,busybox,libselinux1,nfs-common"
		includes="${includes},libpam-ldapd,libnss-ldapd,nscd"
		includes="${includes},screen,since,zsh,vim,aptitude,bzip2,psmisc,less,acpid"
		includes="${includes},dnsutils,pciutils,parted,numactl,hwloc-nox,psmisc,file"
		includes="${includes},ganglia-monitor,ganglia-modules-linux"
		includes="${includes},ntp,rsyslog,openssh-server"
		[ "$INFINIBAND" ] \
			&& includes="${includes},libipathverbs1,qla-tools,infiniband-diags,ibverbs-utils,rdmacm-utils"
		[ "$SLURM" ] && includes="${includes},slurm-llnl"
		excludes="tcpd,xauth,vim-tiny"
		suite="wheezy"

		_printb "#\n# bootstrapping image '${image}' suite '${suite}'\n#\n"

		debootstrap --include=${includes} --exclude=${excludes} ${suite} ${DEBIAN_IMG_DIR}/${image} ${MIRROR}
	fi

	_imgavail ${1}

	if [ -z "${image}" ]
	then
		echo ">> maybe you forgot to pass the bootstrap parameter?"
		return 1
	fi

	_printb "#\n# preparing profiles ...\n#\n"
	echo "> available images : ${images}"
	echo "> selected image(s): ${image}"
	
	for img in ${image}
	do
        rpath="${DEBIAN_IMG_DIR}/${img}"
		_printb "\n> preparing profile ${img} for netboot\n"

		sed -r 's/(\$\{BLUE\}\$\{EXITCODE\}\$\{)WHITE(\}\$\{debian_chroot\:\+\(\$debian_chroot\)\}\$\{RED\})/\1MAGENTA\2/p' \
			/etc/zsh/zshrc > ${rpath}/etc/zsh/zshrc
		cp -a /etc/screenrc ${rpath}/etc/

		echo "> configuring network ..."
        if ! grep -q 'eth0' ${rpath}/etc/network/interfaces
        then
			cat >> ${rpath}/etc/network/interfaces <<- EOF
			# added by prepimgae $(date)
			auto eth0
			iface eth0 inet dhcp
			EOF
            # mysterious curiosities - the above wouldn't work with vims 
			# expandtab option that converts tabs to spaces
		   	# - here-documents need tabs!
        fi

        # facing nfs mount problems at boot?
        test -s "${rpath}/var/run/network/ifstate" \
            && echo -n '' > ${rpath}/var/run/network/ifstate

		# strip the hostname so that it'll be assigned at boot via DHCP
		test -s "${rpath}/etc/hostname" && echo -n '' > ${rpath}/etc/hostname


		# comment out fstab entry for root partition - it'll be added by initramfs-tools
		# sed -i -r 's:(^[a-z0-9].*[[:space:]]+/[[:space:]]+tmpfs[[:space:]]+.*$):# no default root - will be added by initramfs-tools\n# \1:' \
		# 	${rpath}/etc/fstab


		echo "> configuring mount points ..."
		# override fstab of the profile - manual modifications should go to /etc/fstab.d
		cp ${rpath}/etc/fstab ${rpath}/etc/fstab.backup

		cat > ${rpath}/etc/fstab <<- EOF
		proc            /proc           proc    defaults        0       0
		EOF

		if [ -r "${NETBOOT_DIR}/config/templates/fstab" ]
		then
            while read line
            do
                echo "${line}" >> ${rpath}/etc/fstab
                # _printb "> fstab: ${line}\n"
                echo $line | grep -q '^#' && continue
                # _printb "> fstab: # check passed\n"
                mountpoint="$(echo $line | tr '\t' ' ' | tr -s ' ' | cut -d ' ' -f 2)"
                # _printb "> fstab: mountpoint is ${mountpoint}\n"
                if echo "${mountpoint}" | grep -q '^/' \
                    && ! [ -d "${rpath}${mountpoint}" ]
                then
                    # _printb "> fstab: / check passed and directory non-existant\n"
                    printf "> templates.fstab: creating mountpoint ${mountpoint}\n"
                    mkdir -p ${rpath}${mountpoint}
                fi
            done < ${NETBOOT_DIR}/config/templates/fstab
		else
			echo "REMINDER: you can change the fstab template at"
			echo "          ${NETBOOT_DIR}/config/templates/fstab"
			echo "          for them to be automatically added to the profiles fstab"
			# echo "          - please put your profile specific customizations in"
			# echo "          ${rpath}/etc/fstab.d/"
		fi

		if [ "$SLURM" ]
		then
			echo "> configuring scheduler (SLURM) ..."
			cp -a /etc/slurm-llnl/slurm.conf ${rpath}/etc/slurm-llnl/
			cp -a /etc/munge/munge.key ${rpath}/etc/munge/munge.key
			# TODO what about slurm init scripts?
		fi

		if [ "$INFINIBAND" ]
		then
			echo "> configuring infiniband ..."
			for mod in rdma_ucm ib_umad ib_uverbs
			do
		   		grep -q $mod ${rpath}/etc/modules || echo $mod >> ${rpath}/etc/modules
			done
		fi


		echo "> configuring ntp ..."
		master_timezone="$(cat /etc/timezone)"
		image_timezone="$(cat ${rpath}/etc/timezone)"
		if [ "${master_timezone}" != "${image_timezone}" ]
		then
			echo ">> changing images timezone from '${image_timezone}' to '${master_timezone}'."
			cp /etc/timezone ${rpath}/etc/timezone
		fi

		sed -i -r "s/^(server.*debian.pool.ntp.org.*)$/# \1/; \
			s/^(restrict -6 .*)$/# \1/; \
			s/^(restrict ::1.*)$/# \1/" ${rpath}/etc/ntp.conf
		if ! grep -q "server ${MASTER}" ${rpath}/etc/ntp.conf
		then
			echo "server ${MASTER} iburst" >> ${rpath}/etc/ntp.conf
		fi

		echo "> configuring remote logging (rsyslog) ..."
        # rsyslog shall log to the masters daemon
        echo "*.*;auth,authpriv.none          @${MASTER}" \
            > ${rpath}/etc/rsyslog.d/netboot.conf

		echo "> configuring ganglia ..."
		# ganglia configuration
		if ! [ -r "${rpath}/etc/ganglia/conf.d/${MASTER}.conf" ]
		then
			cat > ${rpath}/etc/ganglia/conf.d/${MASTER}.conf <<- EOF
			/* for a detailed configuration file look at /etc/ganglia/gmond.conf */
			cluster {
			    name = "${CLUSTERNAME}"
			}
			host {
			    location = "${CLUSTERNAME}"
			}
			udp_send_channel {
			    host = ${MASTER}
			    port = 8649 
			    ttl = 1
			}
			EOF
		fi


		if [ "$APTPROXY" ]
		then
			echo "> configuring APT proxy ..."
			# configure our apt caching proxy
			echo "Acquire::http::Proxy \"${APTPROXY}\";" > ${rpath}/etc/apt/apt.conf.d/000apt-cacher-ng-proxy
			# and return config to original debian mirror
			echo 'deb http://ftp.us.debian.org/debian wheezy main contrib non-free' > ${rpath}/etc/apt/sources.list
		fi

		
		echo "> configuring passwordless ssh login for root and transfering roots credentials..."
		# set the profiles root account to match the masters one
		sed -i -r "s|^root:.*$|$(grep '^root:' /etc/shadow)|" ${rpath}/etc/shadow

		if [ -r "/root/.ssh/id_rsa.pub" ]
		then
			[ -d "${rpath}/root/.ssh" ] || mkdir -p ${rpath}/root/.ssh
			if ! [ -r "${rpath}/root/.ssh/authorized_keys" ]
			then
				touch ${rpath}/root/.ssh/authorized_keys
				chmod 600 ${rpath}/root/.ssh/authorized_keys
			fi
			key="$(cut -d ' ' -f 2 < /root/.ssh/id_rsa.pub)"

			if ! grep -q ${key} ${rpath}/root/.ssh/authorized_keys
			then
				cat /root/.ssh/id_rsa.pub >> ${rpath}/root/.ssh/authorized_keys
			fi
		else
			_printb "\n>> WARNING: no ssh keys found! passwordless login won't be available.\n\n"
		fi
		imgsshkey "${img}"

		echo "> configuring ldap authentication ..."
		# relevant files /etc/nsswitch.conf /etc/nslcd.conf
		if ! grep -q ldap ${rpath}/etc/nsswitch.conf
		then
			for service in passwd group shadow hosts netgroup
			do
				sed -i -r "s/^(${service}: .*)$/\1 ldap/" ${rpath}/etc/nsswitch.conf
			done
		fi
		if ! grep -q ${BASEDN} ${rpath}/etc/nslcd.conf
		then
			sed -i -r "s/^base .*$/base ${BASEDN}/;\
				s|^uri .*$|uri ldap://${MASTER}/|" ${rpath}/etc/nslcd.conf
		fi


		# update all relevant files
		if [ -d "${CONFIG_PATH}" ]
		then
			echo "> setting up profile for network boot via mcat ..."
			# nodes shall say hello when their boot finished
			# cp -u ${CONFIG_PATH}/etc_init.d_mcatinfo ${rpath}/etc/init.d/mcatinfo
			sed -r "s/^MASTER=.*$/MASTER=${MASTER}/;s/^APPPATH=.*$/APPPATH=mcat/" \
			       	${CONFIG_PATH}/etc_init.d_mcatinfo \
				    > ${rpath}/etc/init.d/mcatinfo
			chmod a+x ${rpath}/etc/init.d/mcatinfo
			/usr/sbin/chroot ${rpath} /usr/sbin/update-rc.d mcatinfo defaults

			# insert initial ramdisk hook - download the profile, unpack it ...
			cp -a ${CONFIG_PATH}/etc_initramfs-tools_scripts_local-bottom_download \
				${rpath}/etc/initramfs-tools/scripts/local-bottom/download
			cp -a ${CONFIG_PATH}/etc_initramfs-tools_scripts_local-premount_myrootfs \
				${rpath}/etc/initramfs-tools/scripts/local-premount/myrootfs
			cp -a ${CONFIG_PATH}/etc_initramfs-tools_hooks_local \
				${rpath}/etc/initramfs-tools/hooks/local
			sed -r -i "s|^MODULES=.*$|MODULES=netboot|;
				   s|^COMPRESS=.*$|COMPRESS=xz|;
				   s|^DEVICE=.*$|DEVICE=eth0|;" ${rpath}/etc/initramfs-tools/initramfs.conf

			case ${img} in
				[A-Fa-f0-9]*)
					# ok image name starts with hex digit -> comment out parse_numeric.
					# bug report 697017: parsing root=xxxx as hex value
                    echo "> /curing bug with image naming conflict/"
					sed -r -i "s/^(parse_numeric.*)$/# \1/" \
					       	${rpath}/usr/share/initramfs-tools/init
					;;
			esac

			echo "> installing nvidia udev rules ..."
			# copy nvida configuration udev rule
			mkdir -p ${rpath}/usr/local/lib/udev/
			cp -a ${CONFIG_PATH}/usr_local_lib_udev_nvidia-smi \
				${rpath}/usr/local/lib/udev/nvidia-smi
			cp -a ${CONFIG_PATH}/etc_udev_rules.d_99-nvidia.rules \
				${rpath}/etc/udev/rules.d/99-nvidia.rules

		fi
		_printb "> updating initial ramdisk ...\n"
		/usr/sbin/chroot ${rpath} /usr/sbin/update-initramfs -k all -u


		# check whether there was a kernel update and the links became absolute, argh!!!

		if ! [ -L "${rpath}/vmlinuz" ] || ! [ -L "${rpath}/initrd.img" ] || \
			! readlink -esq ${rpath}/vmlinuz | grep -q "^${rpath}" || \
			! readlink -esq ${rpath}/initrd.img | grep -q "^${rpath}"
		then
			echo "> fixing symbolic links to initrd.img and vmlinuz in ${rpath} ..."

			# find the latest kernel
			for k in $(ls -1t ${rpath}/boot/vmlinuz-* 2>/dev/null)
			do
				# vmlinuz-3.2.0-4-amd64
				vers="$(basename ${k} | cut -d '-' -f 2-)"
				# vers = 3.2.0-4-amd64
				initrd="${rpath}/boot/initrd.img-${vers}"
				# initrd = initrd.img-3.2.0-4-amd64
				if [ -r "${initrd}" ]
				then
					kernel="${k}"
					break
				else
					continue
				fi
			done
			if [ -z "${kernel}" ] || [ -z "${initrd}" ]
			then
				_printb "\n!!! no proper kernel/initrd found !!!\n\n"
			else
				echo "> setting profile '${img}' to boot kernel version '${vers}'"
				rm -f ${rpath}/vmlinuz ${rpath}/initrd.img
				ln -s boot/initrd.img-${vers} ${rpath}/initrd.img 
				ln -s boot/vmlinuz-${vers} ${rpath}/vmlinuz
			fi
		fi

		_printb "\n> do not forget to pack the image.\n"
	done
}


packimage ()
	# pack a image to be booted via network
{

	images=""


	if [ -z "$1" ]
	then

		echo -n "Usage: "
		echo "$0 <image> [compression level]"
		echo "           where 'compression level' can be one of best|fast|extreme|-<level>"
		echo "           where 'image' must be one of the images in ${DEBIAN_IMG_DIR}"
		echo "           it's also possible to give 'all' which prepares all images at ${DEBIAN_IMG_DIR}"
		echo "           currently these images are available: ${images}"
		_imgavail
		echo "               ${images}"
		return 1
	fi
	if ! [ -d "${DEBIAN_IMG_DIR}" ]
	then
		echo "image dir ${DEBIAN_IMG_DIR} not found"
		return 1
	fi

	case "$2" in
		(best)
			cl="-6"
			;;
		(fast)
			cl="-0"
			;;
		(extreme)
			cl="-9"
			;;
		('')
			cl="-0"
			;;
		('-'*)
			if [ "${2#-}" -le 9 ] && [ "${2#-}" -ge 0 ]
			then
				cl="$2"
			else
				echo "unsupported compression level '$2'"
				return 0
			fi
			;;
		(*)
			echo "compression level '$2' not recognized. try one of best|fast|extreme|-<level>"
			return 1
			;;
	esac



	if [ ${1} = "all" ]
	then
		_imgavail
		image="${images}"
	else
		if ! [ -d "${DEBIAN_IMG_DIR}/${1}" ]
		then
			echo "image '$1' not found in image dir ${DEBIAN_IMG_DIR}"
			_imgavail
			echo "available images are: ${images}"
			return 1
		fi
		image="$1"
	fi


	backuppwd="$(pwd)"
	
	for img in ${image}
	do
		if [ -x "${DEBIAN_IMG_DIR}/${img}/usr/bin/apt-get" ]
		then
			echo -n "cleaning up debian packages ... "
			/usr/sbin/chroot ${DEBIAN_IMG_DIR}/${img} apt-get clean
			echo "done."
		else
			echo "no apt-get - no cleanup."
		fi
		rm -f ${DEBIAN_IMG_DIR}/${img}/etc/debian_chroot

		cd ${DEBIAN_IMG_DIR}/${img}
		echo -n "packing image '${img}' ... "
		
		

		echo "profile=${img}" > ${DEBIAN_IMG_DIR}/${img}/etc/debian_cluster
		
		# insert message of the day hook for our profile
		if [ -r "${DEBIAN_IMG_DIR}/${img}/etc/motd.tail" ]
		then
			motd="${DEBIAN_IMG_DIR}/${img}/etc/motd.tail"
		else
			motd="${DEBIAN_IMG_DIR}/${img}/etc/motd"
		fi
		cp -a ${motd} ${motd}.orig

		echo "\n\nDebian netboot image : ${img}\n\n" >> ${motd}

		# echo "executing: find . -depth -xdev -print | cpio -H crc -o | xz ${cl:--0} > ../${img}.xz"
		find . -depth -xdev -print | cpio -H crc -o | xz ${cl:--0} > ../${img}.xz
		
		# remove message of the day hook
		mv ${motd}.orig ${motd}

		rm -f ${DEBIAN_IMG_DIR}/${img}/etc/debian_cluster

		ls -lh ${DEBIAN_IMG_DIR}/${img}.xz | cut -d ' ' -f 5
	done
	cd ${backuppwd}
}


imgsshkey ()
	# copy ssh rsa/dsa host keys from profile $1 to profile $2 or all
{
	if [ -z "${1}" -o "${1}" = "-h" -o "${1}" = "--help" ]
	then
		cat <<- EOF
		Usage: $(basename ${0}) [source_profile] <dest_profile>
		    where $(basename ${0}) copies ssh host keys from profile <source> 
		    to profile <dest>. dest can also be 'all' in order to synchronize 
		    all keys of profile <source> to all available profiles/images.
			if the host keys in all profiles are the same [source] can be 
		    omitted.
		EOF
		return 1
	fi

	keypath="etc/ssh"

	if [ -z "${2}" ]
	then
		_imgavail ${1}
		if [ -z "${image}" ] || [ "${1}" = "all" ]
		then
			echo ">> destination profile '${1}' (${image}) not found."
			return 6
		fi
		target="${1}"
		
		# find the keys of the most recent profile as source
		num_dsa="$(find ${DEBIAN_IMG_DIR}/*/${keypath}/ \
			-not -path "${DEBIAN_IMG_DIR}/${target}/${keypath}/*" \
		    -name ssh_host_dsa_key \
		    -exec sha1sum {} \; \
		    | cut -d ' ' -f 1 | sort -u | wc -l)"

		num_rsa="$(find ${DEBIAN_IMG_DIR}/*/${keypath}/ \
			-not -path "${DEBIAN_IMG_DIR}/${target}/${keypath}/*" \
		    -name ssh_host_rsa_key \
		    -exec sha1sum {} \; \
		    | cut -d ' ' -f 1 | sort -u | wc -l)"


		if [ "${num_dsa}" -eq 0 -a "${num_rsa}" -eq 0 ]
		then
			# no host keys at all, first profile?
			return 0
		elif [ "${num_dsa}" -eq 1 -a "${num_rsa}" -eq 1 ]
		then
			echo "> all ssh host keys are the same ... synchronizing to profile ${target}."
			# choose one
			source="$(find ${DEBIAN_IMG_DIR} -mindepth 1 -maxdepth 1 -not -name ${target} -type d -printf '%f\n' | head -n 1)"
		else
			echo ">> too many different ssh host keys - you need to take care for them by yourself"
			return 7
		fi
	else
		_imgavail ${2}
		test [ -z "${image}" ] && echo ">> target image '${2}' (${image}) not found." && return 4
		target="${image}"
		_imgavail ${1}
		test [ -z "${image}" ] && echo ">> source image '${1}' (${image}) not found." && return 3
		source="${1}"
	fi


	test "${source}" = "${target}" && echo ">> source and target may not be identical." && return 2


	if ! ls ${DEBIAN_IMG_DIR}/${source}/${keypath}/ssh_host_* >/dev/null 2>&1 
	then
		echo ">> no host keys found at ${DEBIAN_IMG_DIR}/${source}/${keypath}/"
		return 5
	fi


	for img in ${target}
	do
		test "${img}" = "${source}" && continue

		echo "> copying ssh host keys from '${source}' to '${img}' ... "

		if ! [ -d "${DEBIAN_IMG_DIR}/${img}/${keypath}" ]
		then
			echo ">> no ssh directory found for image '${img}'"
			continue
		fi

		for key in ${DEBIAN_IMG_DIR}/${source}/${keypath}/ssh_host_*
		do
			rm -f "${DEBIAN_IMG_DIR}/${img}/${keypath}/ssh_host_*orig"
			cp -a --backup --suffix=".orig" ${key} "${DEBIAN_IMG_DIR}/${img}/${keypath}"
		done
	done

}


mcat ()
	# brief description of available mcat tools
{
		_printb "\n   ===   mcat tools   ===   \n\n"
		# grep -A1 -E '^[a-z]{3,}[[:space:]]+\(\)' $0
		# echo "===   sed   ==="
		# sed -r -n -e '/^([a-z]+[a-zA-Z0-9_-]*)[[:space:]]*\(\)/{N; s/\n//;}' \
		# 	-e 's/^([a-z]+[a-zA-Z0-9_-]*)[[:space:]]*\(\)[[:space:]]+.[[:space:]]+(.*)$/\1 - \2/p' $0
		prefix="           - "
		while read line
		do
			if echo ${line} | grep -qE '^[a-z]{3,}[[:space:]]+\(\)'
			then
				cmd="$(echo ${line} | sed -r 's/^([a-z]+[a-zA-Z0-9_-]*)[[:space:]]+\(\)/\1/')"
				prefix="$(echo "${prefix}" | sed "s/.\{$((${#cmd} - 1))\}/${cmd}/")"
			fi

			if [ "${cmd}" ] && echo ${line} | grep -qE '^[[:space:]]*#'
			then
				echo "${prefix} $(echo "${line}" | sed -r "s/^[[:space:]]*#[[:space:]]*//")"
				prefix="             "
			fi

			if echo ${line} | grep -qE '^[[:space:]]*\{'
			then
				unset cmd
				prefix="           - "
			fi
		done < $0

		echo
		echo "> run the command to see its invocation details"
}


case "$(basename $0)" in
	schrootconf)
		schrootconf
		;;
	imgsshkey)
		imgsshkey $@
		;;
	packimage)
		packimage $@
		;;
	prepimage)
		prepimage $@
		;;
	enterimage)
		enterimage $@
		;;
	nodelist)
		nodelist $@
		;;
	nodestat|nodestatus)
		nodestat $@
		;;
	nodepower)
		nodepower $@
		;;
	pslurp)
		pslurp $@
		;;
	pnuke)
		pnuke $@
		;;
	prsync)
		prsync $@
		;;
	pscp)
		pscp $@
		;;
	pssh|psh)
		pssh $@
		;;
	mcat|mcat.sh)
		mcat $@
		;;
	'')
	   	echo "mcat Copyright (C) 2012 Gabriel Klawitter"
		echo "This program comes with ABSOLUTELY NO WARRANTY."
		echo "This is free software, and you are welcome to"
	   	echo "redistribute it under certain conditions."

		exit 0
		;;
	*)
		echo "Command not found '$0'"
		exit 1
		;;
esac


# vim: tw=78 fo=w2tqc ts=4 ft=sh shiftwidth=4
