
**mcat** (modest clustser administration toolkit) is a _lightweight software 
stack_ for deploying Debian based clusters via netboot. It was designed for
_HPC_ (high-performance computing) clusters with minimal performance impact in
mind.

mcat consists of a set of _commandline tools_ for creating and managing netboot
images as well as simplifying parallel execution of commands on groups of nodes.

All information about the nodes (i.e. MAC addresses or boot image selection) is
stored in a _LDAP directory_ which is comfortably managable through a 
_lightweight web application_. This web application is realized using 
Python/Flask/WSGI and directly creates host configurations for dnsmasq (for 
DNS and DHCP) and also provides a boot menue over HTTP either for nodes 
booting gpxe or grub.


The name mcat is an alteration of 
[_xCAT_](http://sourceforge.net/projects/xcat/) which is a more extensive 
cluster administration toolkit written in Perl.

