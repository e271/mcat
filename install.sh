#!/bin/sh
# 
#     mcat - modest cluster administration toolkit
#     Copyright (C) 2012 Gabriel Klawitter
# 
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#                         __ _                    _   _          
#            __ ___ _ _  / _(_)__ _ _  _ _ _ __ _| |_(_)___ _ _  
#           / _/ _ \ ' \|  _| / _` | || | '_/ _` |  _| / _ \ ' \ 
#           \__\___/_||_|_| |_\__, |\_,_|_| \__,_|\__|_\___/_||_|
#                             |___/                              
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# root of mcat installation
NETBOOT_DIR="/var/netboot"

# cluster facing ip address
MASTER_IP="10.0.0.253"

# cluster internal network
NETWORK="10.0.0.0/16"

# local area network
# will grant access to apache from within it
# disable by specifiing ext. ip address only
LAN="172.16.0.0/16"

DNS_FORWARDER="172.16.110.110"

# this is the path where the webserver offers the application
MCAT_APP_DIR="mcat"
# configuration file of mcat
MCAT_CONFIG="/etc/mcatconfig.py"
HOSTNAME="$(hostname -f)"
DOMAIN="$(hostname -d)"

# ldap configuration
DN="dc=debian,dc=cluster"

# this is basically used for mcat web application authentication
OU_USER="ou=people,dc=cluster,dc=cat"
OU_GROUP="ou=groups,dc=cluster,dc=cat"
MCAT_MANAGER_GROUP="manager"

TEMPLATES="${NETBOOT_DIR}/config/templates"
CONFIG_IMAGES="${NETBOOT_DIR}/config/images"

# comment out environment variable to disable xcat features
XCAT_ENABLED="True"

# just leave it - it never worked yet
GRUBEFI="False"


LDAP_SECRET_FILE="/etc/ldapadmin.secret"


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if [ -t 1 ]
then
        # running in a terminal - argument is the fd
        BOLD='\033[01m'
        DEF='\033[00m'
        EBOLD='\x1b[01m'
        EDEF='\x1b[00m'
fi

printb () {
	printf "${BOLD}${*}${DEF}"
}

all ()
{
	echo -n "Available targets: "
	sed -r -n 's/^([a-z]*) \(\)$/\1/p' $0 | sed ':a;N;$!ba;s/\n/ /g'
}


# target: zsh - Configure the Zsh
zsh ()
{
	printb "#\n# installing and configuring the zsh\n#\n"
	apt-get -qqy install zsh screen since
	[ -r "/etc/zsh/zshrc.orig" ] \
		|| mv --verbose --no-clobber /etc/zsh/zshrc /etc/zsh/zshrc.orig
	[ -d "/etc/zsh/completion" ] || mkdir -p /etc/zsh/completion
	cp -uv etc/zsh/completion/* /etc/zsh/completion
	cp -uv etc/zsh/zshrc.local /etc/zsh/
	echo "> downloading configuration files from grml.org"
	wget -O /etc/zsh/zshrc http://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
	wget -O /etc/skel/.zshrc http://git.grml.org/f/grml-etc-core/etc/skel/.zshrc
	if [ -r "/etc/zsh/zshrc" ] && [ -r "/etc/zsh/zshrc.local" ] \
		&& [ -r "/etc/skel/.zshrc" ]
	then
		echo "> installation successful"
	else
		echo "> installation unsuccessful - some files were not set up correctly"
	fi
}


# target: tools - Install tools like packimage and parallel shell commands
tools ()
{
	apt-get -qq -y install pssh debootstrap schroot
	cp -u usr/local/lib/mcat.sh /usr/local/lib
	chmod a+rx /usr/local/lib/mcat.sh
	# find /usr/local/bin /usr/local/sbin -type l
	ln -sf ../lib/mcat.sh /usr/local/bin/mcat
	ln -sf ../lib/mcat.sh /usr/local/bin/psh
	ln -sf ../lib/mcat.sh /usr/local/bin/pssh
	ln -sf ../lib/mcat.sh /usr/local/bin/pscp
	ln -sf ../lib/mcat.sh /usr/local/bin/pnuke
	ln -sf ../lib/mcat.sh /usr/local/bin/prsync
	ln -sf ../lib/mcat.sh /usr/local/bin/pslurp
	ln -sf ../lib/mcat.sh /usr/local/bin/nodelist
	ln -sf ../lib/mcat.sh /usr/local/sbin/nodestat
	ln -sf ../lib/mcat.sh /usr/local/sbin/nodepower
	ln -sf ../lib/mcat.sh /usr/local/sbin/enterimage
	ln -sf ../lib/mcat.sh /usr/local/sbin/prepimage
	ln -sf ../lib/mcat.sh /usr/local/sbin/packimage
	ln -sf ../lib/mcat.sh /usr/local/sbin/imgsshkey
	ln -sf ../lib/mcat.sh /usr/local/sbin/schrootconf
	cf="/etc/default/mcat"
	if [ -s "${cf}" ]
	then
		if grep -n '|' .${cf} ${cf}
		then
			printb "> substitution parameter \'|\' detected in configuration file ${cf}"
			printb "> installation aborted"
			return 1
		fi
		# try not to override already configured values
		t="$(mktemp)"
		printb "> preserving the following configuration variables in ${cf}:\n"
		echo "------------------------------------------------------------------------------"
		# 1. get those already assigned values and those commented out
		# 2. check for disabled features i.e. those variables that are commented out.
		# 3. check for obsolete configuration variables

		# either so
		# sed -r -n -e '/^[A-Z_-]*=.*$/p' -e '/^[[:space:]]*#+[[:space:]]*[A-Z_-]*=.*$/p'
		# or so: double quotes needed for in string line break
		sed -r -n "/^[A-Z_-]+=.*$/p; \
			/^[[:space:]]*#+[[:space:]#]*[A-Z_-]+=.*$/p" ${cf} \
			| while read line
		do
			pattern="$(echo ${line} | sed -r 's/^[[:space:]#]*([A-Z_-]+)=.*/\1/')"
			if grep -q "${pattern}" .${cf}
			then
				echo "> ${line}"
			else
				echo ">> dropping obsolete ${line}"
			fi
		done
		echo "------------------------------------------------------------------------------"

		sed -r -n "s/^([A-Z_-]+)=(.*)$/s|^\1=.*$|\1=\2|/pg; \
			s/^[[:space:]]*#+[[:space:]#]*([A-Z_-]+)=(.*)$/s|^[[:space:]#]*\1=.*|# \1=\2|/pg" \
		       ${cf} >> $t


		# apply them to the newly to be installed cf=/etc/default/mcat
		sed -r --file=${t} .${cf} > ${cf}
		rm -f $t
	else
		sed "s:^NETBOOT_DIR=.*$:NETBOOT_DIR=${NETBOOT_DIR}:" .${cf} > ${cf}
	fi
	# cp -u etc/default/mcat /etc/default/mcat
	mkdir -p ${CONFIG_IMAGES}
	# TODO why not adapt these file right here?
	cp -a .${CONFIG_IMAGES}/* ${CONFIG_IMAGES}/
	printb "> tools installed successfully\n"
}


# target: mcat - Install mcat application
mcat ()
{
	[ "${DN}" ] || DN="$(ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b "olcDatabase={1}hdb,cn=config" olcSuffix | sed -n 's/^olcSuffix: \(dc=.*\)$/\1/p')"

	printb "#\n# installation of mcat to ${NETBOOT_DIR}/mcat\n#\n"
	[ -d "${NETBOOT_DIR}" ] || mkdir -p ${NETBOOT_DIR}
	cp -ru ./var/netboot/mcat ${NETBOOT_DIR}
	sed -i -r "s:^app.config.from_pyfile\('.*'\)$:app.config.from_pyfile('${MCAT_CONFIG}'):" \
	       	${NETBOOT_DIR}/mcat/mcat.py

	if ! id mcat 2>&1 > /dev/null
	then
		adduser --system --shell /bin/false \
			--home /var/netboot/mcat --group \
			--disabled-login \
			--gecos "mCAT web application" mcat
	fi

	if [ -r "${MCAT_CONFIG}" ]
	then
		printb "> configuration of mcat at ${MCAT_CONFIG} will be preserved\n"
	else
		printb "> writing configuration of mcat to ${MCAT_CONFIG}\n"
		sed -r "s|^DOMAIN.*$|DOMAIN = '${DOMAIN}'|;
			s|dc=debian,dc=cluster|${DN}|;
			s|^LDAP_ADMIN_PASS.*$|LDAP_ADMIN_PASS = '$(test -f ${LDAP_SECRET_FILE} && cat ${LDAP_SECRET_FILE} || echo "")'|;
			s|^PROFILE_DIR.*$|PROFILE_DIR = '${NETBOOT_DIR}/images'|" \
		       	${NETBOOT_DIR}/mcat/config.py > ${MCAT_CONFIG}
		chown root:mcat ${MCAT_CONFIG}
		chmod 640 ${MCAT_CONFIG}

		# maybe check for added configuration variables
	fi


	touch /var/log/mcat.log
	chown mcat:adm /var/log/mcat.log
	apt-get -qqy install python-flask python-wtforms python-psycopg2 libapache2-mod-wsgi python-ldap

	cp -u etc/logrotate.d/mcat /etc/logrotate.d/mcat

	# sed "s|/var/netboot|${NETBOOT_DIR}|" etc/apache2/conf.d/mcat.conf \
	#        	> /etc/apache2/conf.d/mcat.conf
	cat > /etc/apache2/conf.d/mcat.conf <<- EOF

	# configuration of the mcat Machine Maintenance Wep Application

	WSGIDaemonProcess mcat user=mcat group=mcat threads=5
	WSGIScriptAlias /mcat ${NETBOOT_DIR}/mcat/mcat.wsgi
	
	<Directory ${NETBOOT_DIR}/mcat>
	    WSGIProcessGroup		mcat
	    WSGIApplicationGroup 	%{GLOBAL}
	    Order 			Deny,Allow
	    Deny 			from All
	    Allow 			from 127.0.0.1
	    Allow 			from ${NETWORK}
	    Allow 			from ${LAN}
	    
	    # # this will not work - since nodes need to access files without authentication
	    # but maybe you want to restrict http access for other directories
	    # AuthType			Basic
	    # AuthBasicProvider		ldap
	    # AuthName			"mcat Machine Maintenance"
	    # # AuthLDAPURL		ldap://localhost/${OU_USER}?uid
	    # # connect using socket
	    # AuthLDAPURL			ldapi:///${OU_USER}?uid
	    # AuthLDAPGroupAttribute	memberUid
	    # AuthLDAPGroupAttributeIsDN	off
	    # Require			ldap-group cn=${MCAT_MANAGER_GROUP},${OU_GROUP}
	</Directory>

	Alias /static/ ${NETBOOT_DIR}/mcat/static/
	EOF

	sed "s:/var/netboot:${NETBOOT_DIR}:g; s:10.0.0.0/16:${NETWORK}:g; s:172.16.0.0/16:${LAN}:g" \
		etc/apache2/conf.d/netboot.conf > /etc/apache2/conf.d/netboot.conf
	
	test -e /etc/apache2/mods-enabled/authnz_ldap.load || a2enmod authnz_ldap
	service apache2 restart
	if ! grep -q 'mcat.*dnsmasq.*kill' /etc/sudoers
	then
		echo "> don't forget to add sudo entry:"
		echo "> mcat $(hostname)=(dnsmasq)NOPASSWD: /bin/kill"
		echo "> don't forget to disable xcat if you're not using it"
	fi

	if ! [ -r "/var/www/favicon.ico" ]
	then
		wget -O /var/www/favicon.ico http://www.debian.org/favicon.ico
	fi
}



# target: netboot - Setup /var/netboot
netboot ()
{
	mkdir -p ${NETBOOT_DIR}/images ${NETBOOT_DIR}/tftp
	apt-get -q -y install ipxe ntp dnsmasq nfs-kernel-server
	ln -sf /usr/lib/ipxe/undionly.kpxe ${NETBOOT_DIR}/tftp/undionly.kpxe
	sed -r -i 's|^CONFIG_DIR=.*$|CONFIG_DIR=/etc/dnsmasq.d,.dpkg-dist,.dpkg-old,.dpkg-new,.hosts|' /etc/default/dnsmasq
	touch /etc/dnsmasq.d/mcat.conf /etc/dnsmasq.d/mcat.hosts
	chown mcat:mcat /etc/dnsmasq.d/mcat.conf /etc/dnsmasq.d/mcat.hosts
	cp etc/dnsmasq.d/dnsmasq.conf /etc/dnsmasq.d/dnsmasq.conf
	cat >> /etc/dnsmasq.d/dnsmasq.conf <<- EOF
	server=${DNS_FORWARDER}
	interface=eth0
	interface=eth1
	no-dhcp-interface=eth1
	domain=${DOMAIN}
	# dhcp-boot=tag:gpxe,http://${HOSTNAME}/${MCAT_APP_DIR}/netboot/gpxe.cfg
	dhcp-boot=tag:gpxe,http://${MASTER_IP}/${MCAT_APP_DIR}/netboot/gpxe.cfg
	tftp-root=${NETBOOT_DIR}/tftp
	dhcp-script=${NETBOOT_DIR}/mcat/mcat.py
	EOF
	chown -R root:mcat ${NETBOOT_DIR}/mcat
	chmod ug+x ${NETBOOT_DIR}/mcat/mcat.py

	service dnsmasq restart
	wget -O - --quiet http://localhost/${MCAT_APP_DIR}/dnsmasq/refresh
	cat > /etc/resolv.conf <<- EOF
	domain ${DOMAIN}
	search ${DOMAIN}
	nameserver ${MASTER_IP}
	EOF

	# these nfs exports may match with /var/netboot/config/templates/fstab
	mkdir -p ${NETBOOT_DIR}/config/templates
	FSTAB_TPL="${NETBOOT_DIR}/config/templates/fstab"
	touch ${FSTAB_TPL}
	if ! grep -q '^/home' /etc/exports
	then
		echo "/home                   ${NETWORK}(rw,async,no_subtree_check,no_root_squash)" >> /etc/exports
	fi
	if ! grep -q "^${HOSTNAME}:/home" ${FSTAB_TPL}
	then
		echo "${HOSTNAME}:/home           /home           nfs             defaults        0       0" >> ${FSTAB_TPL}
	fi
	# best path?
	if ! grep -q '^/opt/software' /etc/exports
	then
		mkdir -p /opt/software
		echo "/opt/software           ${NETWORK}(rw,async,no_subtree_check,no_root_squash)" >> /etc/exports
	fi
	if ! grep -q "^${HOSTNAME}:/opt/software" ${FSTAB_TPL}
	then
		echo "${HOSTNAME}:/opt/software   /opt/software   nfs             defaults        0       0" >> ${FSTAB_TPL}
	fi

	sed -r -i 's/^(udp6 .*)$/# \1/; s/^(tcp6 .*)$/# \1/' /etc/netconfig
	service rpcbind restart
	service nfs-common restart
	service nfs-kernel-server restart

	cat > /etc/rsyslog.d/netboot.conf <<- EOF
	# this file was created by mcat install script (section netboot)
	# this configures rsyslog to listen on the cluster facing interface
	# for syslog messages from the nodes
	\$ModLoad imudp
	\$UDPServerAddress ${MASTER_IP}
	\$UDPServerRun 514
	EOF
	service rsyslog restart
}




# target: ldap - Install and configure the LDAP server
ldap ()
{
	apg -n 1 -m 10 | tr -d '\n' > ${LDAP_SECRET_FILE}
	chmod 6400 ${LDAP_SECRET_FILE}
	DEBIAN_FRONTEND='noninteractive' apt-get install -yq slapd
	apt-get -yq install ldap-utils ldapscripts
	mkdir -p /etc/ldap ${TEMPLATES}
	# modify root password
	PASS="$(slappasswd -T ${LDAP_SECRET_FILE})"
	[ "${DN}" ] || DN="$(ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b "olcDatabase={1}hdb,cn=config" olcSuffix | grep '^olcSuffix:' | cut -d ' ' -f 2)"
	# TODO depends on mcat - templates !!!
	sed -r "s|^olcRootPW:.*$|olcRootPW: ${PASS}|" .${TEMPLATES}/rootpw_mcat.ldif > ${TEMPLATES}/rootpw_mcat.ldif
	ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f ${TEMPLATES}/rootpw_mcat.ldif
	# modify admin password
	sed -r "s|dc=debian,dc=cluster|${DN}|; s|^userPassword:.*$|userPassword: ${PASS}|" .${TEMPLATES}/adminpw_mcat.ldif > ${TEMPLATES}/adminpw_mcat.ldif
	ldapmodify -y ${LDAP_SECRET_FILE} -D "cn=admin,${DN}" -f ${TEMPLATES}/adminpw_mcat.ldif
	# create organisational units
	sed -r "s|dc=debian,dc=cluster|${DN}|" .${TEMPLATES}/ous_mcat.ldif > ${TEMPLATES}/ous_mcat.ldif
	ldapadd -y ${LDAP_SECRET_FILE} -D "cn=admin,${DN}" -f ${TEMPLATES}/ous_mcat.ldif
	# index - see with ldapsearch -Y EXTERNAL -H ldapi:/// -b "olcDatabase={1}hdb,cn=config"
	cp var/netboot/config/templates/index.ldif ${TEMPLATES}/index.ldif
	ldapmodify -QY EXTERNAL -H ldapi:/// -f  ${TEMPLATES}/index.ldif
	chsh --shell /bin/sh openldap
	/etc/init.d/slapd stop
	su -c '/usr/sbin/slapindex -v' openldap
	/etc/init.d/slapd start
	test -f /etc/init.d/nslcd && service nslcd restart
	chsh --shell /bin/false openldap
	# install web frontend
	apt-get -yq install ldap-account-manager php5-mcrypt
	echo "please consider that ldap-account-manager has to be configured manually"
	# TODO configure ldap authentication
}


# target: grub - Configure grub for netboot
grub ()
{
	set -e
	current_path="$(pwd)"
	printb "#\n# install grub from sources:\n#\n"
	printb "> install bazaar\n"
	apt-get -q -y install bzr autogen autoconf bison flex make
	printb "> download, build and install it\n"
	mkdir -p ${NETBOOT_DIR}/src && cd ${NETBOOT_DIR}/src/
	if [ -d "${NETBOOT_DIR}/src/grub" ]
	then
		cd ${NETBOOT_DIR}/src/grub
		current_revision="$(bzr version-info | sed -n 's/revno: \([0-9]*\)/\1/p')"
		bzr pull
		new_revision="$(bzr version-info | sed -n 's/revno: \([0-9]*\)/\1/p')"
	else
		bzr branch http://bzr.savannah.gnu.org/r/grub/trunk/grub
		cd ${NETBOOT_DIR}/src/grub
	fi

	if [ -z "${current_revision}" -o -z "${new_revision}" ] || [ "${current_revision}" -ne "${new_revision}" ]
	then
		./autogen.sh
		if [ "${GRUBEFI}" = "True" ]
		then
			./configure --prefix=${NETBOOT_DIR}/install/grub --with-platform=efi --target=x86_64
		else
			./configure --prefix=${NETBOOT_DIR}/install/grub
		fi
		make && make install
	fi

	mkdir -p ${NETBOOT_DIR}/grub && cd ${NETBOOT_DIR}/grub


	grub_modules='pxe net http extcmd echo boot crypto terminal gettext normal configfile'
	# grub_modules='pxe net http extcmd echo boot crypto terminal gettext normal configfile sleep terminfo'

	printb "\n> configure HTTP netboot \n\n"
	cat > early.cfg <<- EOF
	set color_normal=blue/black
	echo '                                                       _|    '
	echo '                 _|_|_|  _|_|      _|_|_|    _|_|_|  _|_|_|_|'
	echo '                 _|    _|    _|  _|        _|    _|    _|    '
	echo '                 _|    _|    _|  _|        _|    _|    _|    '
	echo '                 _|    _|    _|    _|_|_|    _|_|_|      _|_|'
	set color_normal=white/black
	echo '                 --- modest cluster administration toolkit ---'
	echo ''
	echo 'perform a bootp autoconfiguration'
	net_bootp
	echo 'finished early grub initialisation'
	EOF
	# need dns? problems?
	# echo 'DNS server ${MASTER_IP}'
	# net_add_dns ${MASTER_IP}

	### this is obsolete since grub now fetches it's /grub/grub.cfg first
	# echo 'Calling mcat for configuration file.'
	# configfile      /${MCAT_APP_DIR}/netboot/grub.cfg

	# echo "configfile      /${MCAT_APP_DIR}/netboot/grub.cfg" > ${NETBOOT_DIR}/grub/grub.cfg


	cat > grub.cfg <<- EOF
	set default=0
	set timeout=10

	terminal_output gfxterm

	set apppath="/${MCAT_APP_DIR}"
	set root="(http,${MASTER_IP})"

	set menu_color_normal=cyan/blue
	set menu_color_highlight=white/blue
	
	if [ -s "/${MCAT_APP_DIR}/netboot/default.cfg" ]
	then
	    # fetch nodes configured profile menuentry
	    source /${MCAT_APP_DIR}/netboot/default.cfg
	fi

	submenu "> Debian cluster profiles" {
	    configfile /${MCAT_APP_DIR}/netboot/profiles.cfg
	}
	EOF

	if [ "${XCAT_ENABLED}" = "True" ]
	then
		cat >> grub.cfg <<- EOF

		submenu "> Centocats xCAT profiles" {
		    configfile /${MCAT_APP_DIR}/netboot/xcat.cfg
		}
		EOF
	fi

	cat >> grub.cfg <<- EOF

	submenu "> Specials" {
	    configfile /grub/specials.cfg
	}

	menuentry "Information" {
	    echo "IP Address"
	    net_ls_addr
	    echo "Routes"
	    net_ls_routes
	    echo "DNS"
	    net_ls_dns
	    echo "Press ENTER to continue"
	    read x
	}

	menuentry "Boot your local harddrive (hd0)" {
	    insmod chain
	    insmod relocator
	    insmod mmap
	    chainloader (hd0)+1
	}

	menuentry "Reboot" {
	    echo "Rebooting your machine ..."
	    reboot
	}

	menuentry "Exit grub" {
	    echo "booting next device ..."
	    exit
	}
	EOF


	# grub-editenv --verbose grubenv set server=\"${MASTER_IP}\"
	if [ "${GRUBEFI}" = "True" ]
	then
		${NETBOOT_DIR}/install/grub/bin/grub-mkimage \
			--format=x86_64-efi \
			--output=grub2pxe.0 \
			--config=early.cfg \
			--prefix="(http,${MASTER_IP})/grub" net http echo
		ln -sf ../install/grub/lib/grub/x86_64-efi
		# try that also: normal chain boot configfile linux multiboot
		# maybe aviod grub.cfg oneliner
	else
		# build gpxe loadable grub2 binary - .0 needed for pxe (else multiboot turned on)
		${NETBOOT_DIR}/install/grub/bin/grub-mkimage \
			--format=i386-pc-pxe \
			--output=grub2pxe.0 \
			--config=early.cfg \
			--prefix="(http,${MASTER_IP})/grub" \
			${grub_modules}

		ln -sf ../install/grub/lib/grub/i386-pc
	fi
	cd ${current_path}
	set +e

}



# target: help - Display callable targets.
help ()
{
	echo "mcat Copyright (C) 2012 Gabriel Klawitter"
	echo "This program comes with ABSOLUTELY NO WARRANTY."
	echo "This is free software, and you are welcome to"
	echo "redistribute it under certain conditions."
	echo
	echo
	echo "Available targets are:"
	sed -r -n 's/^# target:(.*)$/\1/p' $0
}



clean ()
{
	echo 'This is not implemented yet, sorry'
}


case "$1" in
	(zsh)
		zsh
		;;
	(tools)
		tools
		;;
	(netboot)
		netboot
		;;
	(ldap)
		ldap
		;;
	(grub)
		grub
		;;
	(mcat)
		mcat
		;;
	(*)
		help
		;;
esac

